<?php
$query_result = $obj_meetings->select_all_published_meetings_info();
$query_result_news_feed = $obj_meetings->select_all_published_news_feed_info();
$query_result_knowledge_share = $obj_meetings->select_all_published_knowledge_share_info();

?>

<section id="blog">
    <div class="container">
        <div class="section-header">
            <h3 class="section-title text-center wow fadeInDown">News Feed & Knowledge Share</h3>
            <!--<p class="text-center wow fadeInDown">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>-->
        </div>
        <div class="row">

            <div class="col-sm-6">
                <?php
                while ($meetings_info = mysqli_fetch_assoc($query_result)) {
                    extract($meetings_info);
                    ?>
                    <div class="blog-post blog-large wow fadeInLeft" data-wow-duration="300ms" data-wow-delay="0ms">
                        <article>
                            <header class="entry-header">
                                <div class="entry-thumbnail">
                                    <img class="img-responsive" src="<?php echo 'includes/' . $meeting_minutes_image; ?>" alt="Meetings">
                                    <span class="post-format post-format-video"><i class="fa fa-film"></i></span>
                                </div>
                                <div class="entry-date"><?php echo $date; ?></div>
                                <h2 class="entry-title"><a href="#"><?php echo $subject; ?></a></h2>
                            </header>

                            <div class="entry-content">
                                <P><?php echo $short_description; ?></P>
                                <a class="btn btn-primary" href="pm_member/view_meeting_minutes.php?id=<?php echo $meetings_id; ?>">Read More</a>
                            </div>

                            <footer>
                                <form>
                                    <textarea name="comment" style="width: 450px;" placeholder="Leave Your Comment Here"></textarea>
                                    <input type="submit" name="comment" value="Comment">
                                </form>

    <!--                                <span class="entry-author"><i class="fa fa-pencil"></i> <a href="#">Victor</a></span>
    <span class="entry-category"><i class="fa fa-folder-o"></i> <a href="#">Tutorial</a></span>
    <span class="entry-comments"><i class="fa fa-comments-o"></i> <a href="#">15</a></span>-->
                            </footer>
                        </article>
                    </div>
                <?php }; ?>
            </div><!--/.col-sm-6-->
            
            <div class="col-sm-6">
                <?php
                while ($news_feed_info = mysqli_fetch_assoc($query_result_news_feed)) {
                    extract($news_feed_info);
                    ?>
                <div class="blog-post blog-media wow fadeInRight" data-wow-duration="300ms" data-wow-delay="100ms">
                    <article class="media clearfix">
                        <!--                        <div class="entry-thumbnail pull-left">
                                                    <img class="img-responsive" src="assets/frontend/images/blog/02.jpg" alt="">
                                                    <span class="post-format post-format-gallery"><i class="fa fa-image"></i></span>
                                                </div>-->
                        <div class="media-body">
                            <header class="entry-header">
                                <div class="entry-date"><?php echo $news_date_time; ?></div>
                                <h2 class="entry-title"><a href="#"><?php echo $news_title; ?></a></h2>
                            </header>

                            <div class="entry-content">
                                <P><?php echo $news_contents; ?></P>
                                <!--<a class="btn btn-primary" href="#">Read More</a>-->
                            </div>

<!--                            <footer class="entry-meta">
                                <span class="entry-author"><i class="fa fa-pencil"></i> <a href="#">Campbell</a></span>
                                <span class="entry-category"><i class="fa fa-folder-o"></i> <a href="#">Tutorial</a></span>
                                <span class="entry-comments"><i class="fa fa-comments-o"></i> <a href="#">15</a></span>
                            </footer>-->
                        </div>
                    </article>
                </div>
                <?php };?>
                <div class="entry-date text-center text-capitalize">
                    <h3 class="text-warning">Knowledge Share</h3>
                    </hr>
                </div>
                
                <?php
                while ($knowledge_share = mysqli_fetch_assoc($query_result_knowledge_share)) {
                    extract($knowledge_share);
                    ?>
                <div class="blog-post blog-media wow fadeInRight" data-wow-duration="300ms" data-wow-delay="200ms">
                    <article class="media clearfix">
<!--                        <div class="entry-thumbnail pull-left">
                            <img class="img-responsive" src="assets/frontend/images/blog/03.jpg" alt="">
                            <span class="post-format post-format-audio"><i class="fa fa-music"></i></span>
                        </div>-->
                        
                        <div class="media-body">
                            <header class="entry-header">
                                
                                <div class="entry-date"><?php echo $date_time; ?></div>
                                <h2 class="entry-title"><a href="#"><?php echo $knowledge_share_title; ?></a></h2>
                            </header>

                            <div class="entry-content">
                                <h5><?php echo $publisher_name;?></h5>
                                <P><?php echo $knowledge_share_contents?></P>
                                <a class="btn btn-primary" href="<?php echo 'includes/'.$knowledge_share_file; ?>">Read More</a>
                            </div>

<!--                            <footer class="entry-meta">
                                <span class="entry-author"><i class="fa fa-pencil"></i> <a href="#">Ruth</a></span>
                                <span class="entry-category"><i class="fa fa-folder-o"></i> <a href="#">Tutorial</a></span>
                                <span class="entry-comments"><i class="fa fa-comments-o"></i> <a href="#">15</a></span>
                            </footer>-->
                        </div>
                    </article>
                </div>
                <?php };?>
                
            </div>
        </div>

    </div>
</section>