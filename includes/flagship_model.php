
<?php
$query_result = $obj_flagship->select_all_published_flagship();
?>


<?php while ($flagship_info = mysqli_fetch_assoc($query_result)) { ?>
    <section id="features">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown"><?php echo $flagship_info['flagship_title'] . '-' . $flagship_info['flagship_model_name']; ?></h2>
                <p class="text-center wow fadeInDown"><?php echo $flagship_info['short_description']; ?></p>
            </div>
            <div class="row">
                <div class="col-sm-6 wow fadeInLeft">
                    <img class="img-responsive" src="<?php echo 'includes/' . $flagship_info['flagship_image']; ?>" alt="">
                </div>
                <div class="col-sm-6">
                    
                    <div class="embed-responsive embed-responsive-16by9">
                        <?php echo $flagship_info['flagship_video']; ?>
                                       <!-- <iframe width="640" height="360" src="https://www.youtube.com/embed/vUCM_0evdQY" frameborder="0" allowfullscreen></iframe>-->
                    </div>
                    <!--                    <div class="media service-box wow fadeInRight">
                                            <div></div>                    
                                        </div>-->

                    <!--                    <div class="media service-box wow fadeInRight">
                                            <div class="pull-left">
                                                <i class="fa fa-pie-chart"></i>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">SEO Services</h4>
                                                <p>Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater</p>
                                            </div>
                                        </div>
                    
                                        <div class="media service-box wow fadeInRight">
                                            <div class="pull-left">
                                                <i class="fa fa-pie-chart"></i>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">SEO Services</h4>
                                                <p>Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater</p>
                                            </div>
                                        </div>-->
                </div>
            </div>
        </div>
    </section>
<?php }; ?>  
<!--<section id="features">
<div class="container">
<div class="section-header">
    <h2 class="section-title text-center wow fadeInDown">Awesome Features</h2>
    <p class="text-center wow fadeInDown">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
</div>
<div class="row">
    <div class="col-sm-6 wow fadeInLeft">
        <img class="img-responsive" src="assets/frontend/images/main-feature.png" alt="">
    </div>
    <div class="col-sm-6">
        <div class="media service-box wow fadeInRight">
            <div class="pull-left">
                <i class="fa fa-line-chart"></i>
            </div>
            <div class="media-body">
                <h4 class="media-heading">UX design</h4>
                <p>Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater</p>
            </div>
        </div>

        <div class="media service-box wow fadeInRight">
            <div class="pull-left">
                <i class="fa fa-cubes"></i>
            </div>
            <div class="media-body">
                <h4 class="media-heading">UI design</h4>
                <p>Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater</p>
            </div>
        </div>

        <div class="media service-box wow fadeInRight">
            <div class="pull-left">
                <i class="fa fa-pie-chart"></i>
            </div>
            <div class="media-body">
                <h4 class="media-heading">SEO Services</h4>
                <p>Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater</p>
            </div>
        </div>

        <div class="media service-box wow fadeInRight">
            <div class="pull-left">
                <i class="fa fa-pie-chart"></i>
            </div>
            <div class="media-body">
                <h4 class="media-heading">SEO Services</h4>
                <iframe width="200" height="150" src="https://www.youtube.com/embed/oodStbP77Qk" frameborder="0" allowfullscreen></iframe>
                <p></p>
            </div>
        </div>
    </div>
</div>
</div>
</section>-->