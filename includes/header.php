<header id="header">
    <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html"><img src="assets/images/edison-logo.png" alt="logo" style="height: 60px; width: 120px;"></a>
            </div>

            <div class="collapse navbar-collapse navbar-right">
                <ul class="nav navbar-nav">
                    <li class="scroll active"><a href="#home">Home</a></li>
                    <li class="scroll"><a href="#features">Upcoming Models</a></li>
                    <?php
                    if ($_SESSION['pm_member'] == 1) {
                        echo '<li class="scroll"><a href="pm_member/pm_master.php">PM Member</a></li>';
                    };
                    ?>
                    

                    <li class="scroll"><a href="#services">Services</a></li>
                    <li class="scroll"><a href="#portfolio">Projects</a></li>
                    <li class="scroll"><a href="#about">About Us</a></li>
                    <?php
                    if ($_SESSION['pm_member'] == 1) {
                        echo '<li class="scroll"><a href="#blog">News Feed</a></li>';
                    };
                    ?>
                    <li class="scroll"><a href="#get-in-touch">Contact</a></li>                        
                    <li class="scroll">
                        <a class="" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i><?php echo $_SESSION['user_name']; ?><i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="#"><i class="fa fa-user fa-fw"></i> Your Profile</a></li> 
                            <li><a href="?status=logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                        </ul>
                    </li>                        
                </ul>
            </div>
        </div><!--/.container-->
    </nav><!--/nav-->
</header>