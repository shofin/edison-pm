<?php
$query_result = $obj_project_summary->select_all_published_project_image();
$query_result_product_image = $obj_project_summary->select_all_published_project_image();

?>



<section id="portfolio">
    <div class="container">
        <div class="section-header">
            <h2 class="section-title text-center wow fadeInDown">Our Works</h2>
            <!--<p class="text-center wow fadeInDown">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>-->
        </div>

        <div class="text-center">
            <ul class="portfolio-filter">
                <li><a class="active" href="#" data-filter="*">All Works</a></li>
                <?php
                while ($all_models_image = mysqli_fetch_assoc($query_result)) {
                    extract($all_models_image);
                    ?>
                    <li><a href="#" data-filter="<?php echo '.' . $product_category; ?>"><?php echo $product_category.' '.'Category'; ?> </a></li>
                <?php }; ?>
            </ul>
        </div>

        <div class="portfolio-items">
           <?php
                while ($all_product_image = mysqli_fetch_assoc($query_result_product_image)) {
                    extract($all_product_image);
                    ?>
            
            <div class="portfolio-item <?php echo $product_category; ?>">
                <div class="portfolio-item-inner">
                    <img class="img-responsive" src="<?php echo 'include/'.$models_image; ?>" alt="">
                    <div class="portfolio-info">
                        <h3>Portfolio Item 1</h3>
                        Lorem Ipsum Dolor Sit
                        <a class="preview" href="<?php echo 'include/'.$models_image; ?>" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                    </div>
                </div>
            </div>
                <?php };?>
            
        </div>
    </div><!--/.container-->
</section>