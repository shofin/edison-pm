<?php
$query_result = $obj_slider->select_all_published_slider();
?>


<section id="main-slider">
    <div class="owl-carousel">
        <?php while ($slider_info = mysqli_fetch_assoc($query_result)) { ?>
            <div class="item img-rounded" style="background-image: url(<?php echo 'includes/'.$slider_info['slider_image']; ?>)">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h2 class="text text-success"><span><?php echo $slider_info['slider_name'].' -- '.$slider_info['slider_title']; ?></span></h2> 
                                    <p class="text text-primary"><?php echo $slider_info['slider_description']; ?></p>
<!--                                    <a class="btn btn-primary btn-lg" href="#">Read More</a>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          <?php }; ?>       
    </div><!--/.owl-carousel-->
</section>



 