<?php
$query_result = $obj_about_us->select_all_published_about_us_info();
$query_result_team_leader = $obj_team_leader->select_all_published_team_leader_info();
$query_result_pm_member = $obj_pm_member->select_pm_user_info();
?>

<?php while ($about_us_info = mysqli_fetch_assoc($query_result)) { ?>
    <section id="about">
        <div class="container">

            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">About Us</h2>
                <!--<p class="text-center wow fadeInDown"> <?php echo $about_us_info['Content_short_description']; ?></p>-->
            </div>
            <div class="row">
                <div class="col-sm-6 wow fadeInLeft">
                    <h3 class="column-title">Video Intro</h3>
                    <!-- 16:9 aspect ratio -->
                    <div class="embed-responsive embed-responsive-16by9">
                        <?php echo $about_us_info['about_us_video']; ?>
                   <!-- <iframe width="640" height="360" src="https://www.youtube.com/embed/vUCM_0evdQY" frameborder="0" allowfullscreen></iframe>-->
                    </div>
                </div>

                <div class="col-sm-6 wow fadeInRight">
                    <h3 class="column-title">Multi Capability</h3>
                    <p><?php echo $about_us_info['multi_capability']; ?></p>

                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="nostyle">
                                <li><i class="fa fa-check-square"></i> Ipsum is simply dummy</li>
                                <li><i class="fa fa-check-square"></i> When an unknown</li>
                            </ul>
                        </div>

                        <div class="col-sm-6">
                            <ul class="nostyle">
                                <li><i class="fa fa-check-square"></i> The printing and typesetting</li>
                                <li><i class="fa fa-check-square"></i> Lorem Ipsum has been</li>
                            </ul>
                        </div>
                    </div>

                    <a class="btn btn-primary" href="#">Learn More</a>
                <?php }; ?> 
            </div>
        </div>
    </div>
</section><!--/#about-->

<section id="work-process">
    <div class="container">
        <div class="section-header">
            <h2 class="section-title text-center wow fadeInDown">Our Process</h2>
            <p class="text-center wow fadeInDown">We follow international standard, do our best to improve product quality <br> We desire innovation, We made the best</p>
        </div>

        <div class="row text-center">
            <div class="col-md-2 col-md-4 col-xs-6">
                <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                    <div class="icon-circle">
                        <span>1</span>
                        <i class="fa fa-coffee fa-2x"></i>
                    </div>
                    <h3>MEET</h3>
                </div>
            </div>
            <div class="col-md-2 col-md-4 col-xs-6">
                <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">
                    <div class="icon-circle">
                        <span>2</span>
                        <i class="fa fa-bullhorn fa-2x"></i>
                    </div>
                    <h3>PLAN</h3>
                </div>
            </div>
            <div class="col-md-2 col-md-4 col-xs-6">
                <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="200ms">
                    <div class="icon-circle">
                        <span>3</span>
                        <i class="fa fa-image fa-2x"></i>
                    </div>
                    <h3>DESIGN</h3>
                </div>
            </div>
            <div class="col-md-2 col-md-4 col-xs-6">
                <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="300ms">
                    <div class="icon-circle">
                        <span>4</span>
                        <i class="fa fa-heart fa-2x"></i>
                    </div>
                    <h3>DEVELOP</h3>
                </div>
            </div>
            <div class="col-md-2 col-md-4 col-xs-6">
                <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="400ms">
                    <div class="icon-circle">
                        <span>5</span>
                        <i class="fa fa-shopping-cart fa-2x"></i>
                    </div>
                    <h3>TESTING</h3>
                </div>
            </div>
            <div class="col-md-2 col-md-4 col-xs-6">
                <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="500ms">
                    <div class="icon-circle">
                        <span>6</span>
                        <i class="fa fa-space-shuttle fa-2x"></i>
                    </div>
                    <h3>LAUNCH</h3>
                </div>
            </div>
        </div>
    </div>
</section><!--/#work-process-->

<section id="meet-team">
    <div class="container">
        <div class="section-header">
            <h2 class="section-title text-center wow fadeInDown">Meet The Team</h2>
            <p class="text-center wow fadeInDown">We have the best innovative Team LEADER to achieve our goal.<br> Have a look</p>
        </div>

        <?php while ($team_leader_info = mysqli_fetch_assoc($query_result_team_leader)) { ?>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                        <div class="team-img ">
                            <img class="img-responsive img-thumbnail" height="100%" width="100%" src="<?php echo 'includes/' . $team_leader_info['team_leader_image']; ?>" alt="">
                        </div>
                        <div class="team-info">
                            <h3><?php echo $team_leader_info['team_leader_name'] . ' ' . $team_leader_info['nick_name']; ?></h3>
                            <span><?php echo $team_leader_info['team_leader_designation'] . ' ID: 0' . $team_leader_info['employe_id']; ?></span>
                        </div>
                        <p><?php echo $team_leader_info['career_short_description']; ?></p>
                        <ul class="social-icons">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            <?php }; ?> 


        </div>
    </div>
</section>

<section id="testimonial">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">

                <div id="carousel-testimonial" class="carousel slide text-center" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <p><img class="img-circle img-thumbnail" src="assets/frontend/images/testimonial/active_pm_member.jpg" alt=""></p>
                            <h4>Md. Safiul Islam</h4>
                            <small>safiul.islam@edison-bd.com</small>
                            <p>Former junior eng. at Samsung Bangladesh R&D Institute (SRBD)</p>
                        </div>
                        <?php while ($pm_member_info = mysqli_fetch_assoc($query_result_pm_member)) { ?>
                        <div class="item">
                            <p><img class="img-circle img-thumbnail" src="<?php echo $pm_member_info['user_image']; ?>" alt=""></p>
                            <h4><?php echo $pm_member_info['user_name'].' ID: '.$pm_member_info['employe_id']; ?></h4>
                            <small><?php echo $pm_member_info['email_address']; ?><</small>
                            <p><?php echo $pm_member_info['career_short_description']; ?><</p>
                        </div>
                        <?php }; ?> 
                    </div>

                    <!-- Controls -->
                    <div class="btns">
                        <a class="btn btn-primary btn-sm" href="#carousel-testimonial" role="button" data-slide="prev">
                            <span class="fa fa-angle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="btn btn-primary btn-sm" href="#carousel-testimonial" role="button" data-slide="next">
                            <span class="fa fa-angle-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>