-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2017 at 02:14 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_edison-pm`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_about_us`
--

CREATE TABLE `tbl_about_us` (
  `about_us_id` int(3) NOT NULL,
  `Content_short_description` text NOT NULL,
  `multi_capability` text NOT NULL,
  `about_us_video` text NOT NULL,
  `publication_status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_about_us`
--

INSERT INTO `tbl_about_us` (`about_us_id`, `Content_short_description`, `multi_capability`, `about_us_video`, `publication_status`) VALUES
(3, 'bujhtaci na ki komu, vai onek koste kaj kortesi... ', 'We have a vision to make more faster more effective product. we belive we can be the best leading company in this country...................................................................s"think hard work smart"', '<iframe width="854" height="480" src="https://www.youtube.com/embed/oodStbP77Qk" frameborder="0" allowfullscreen></iframe>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(3) NOT NULL,
  `admin_name` varchar(50) NOT NULL,
  `email_address` varchar(50) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_name`, `email_address`, `password`) VALUES
(1, 'Md. Safiul Islam', 'admin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_assets`
--

CREATE TABLE `tbl_assets` (
  `asset_id` int(11) NOT NULL,
  `item_name` text NOT NULL,
  `model_name` text NOT NULL,
  `IMEI` int(20) NOT NULL,
  `author` varchar(30) NOT NULL,
  `carried_by` varchar(30) NOT NULL,
  `date_of_assign` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `purpose` text NOT NULL,
  `remarks` text NOT NULL,
  `date_of_modification` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `category_id` int(3) NOT NULL,
  `category_name` varchar(40) NOT NULL,
  `category_description` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`category_id`, `category_name`, `category_description`, `publication_status`) VALUES
(1, 'Smart Phone', 'All Android Smart Phone.', 1),
(2, 'Feature Phone', 'All Feature Phone', 1),
(3, 'Smart TAB', 'All Smart TAB', 1),
(4, 'Selfie Stick', 'well', 0),
(5, 'Charger', 'Well', 0),
(6, 'Bluetooth Headphone', 'Well', 1),
(7, 'Digital Camera', 'Well', 0),
(8, 'Airphone', 'well', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_common_issue`
--

CREATE TABLE `tbl_common_issue` (
  `common_issue_id` int(3) NOT NULL,
  `project_id` int(3) NOT NULL,
  `project_owner_id` int(3) NOT NULL,
  `odm_id` int(3) NOT NULL,
  `chipset` varchar(10) NOT NULL,
  `subject` text NOT NULL,
  `issue_details` text NOT NULL,
  `remarks` text NOT NULL,
  `added_by` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_common_issue`
--

INSERT INTO `tbl_common_issue` (`common_issue_id`, `project_id`, `project_owner_id`, `odm_id`, `chipset`, `subject`, `issue_details`, `remarks`, `added_by`) VALUES
(1, 3, 5, 2, '333', 'Weekly PMI meeting 333', '3333', '3333', 'Md. Safiul Islam'),
(2, 3, 6, 3, '6580', 'demo', '', '', 'Md. Safiul Islam'),
(3, 4, 6, 4, '33256', 'hi', 'hlo', 'jkhjgxjk', 'Md. Safiul Islam');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_developer`
--

CREATE TABLE `tbl_developer` (
  `developer_id` int(2) NOT NULL,
  `developer_name` text NOT NULL,
  `developer_email_address` varchar(30) NOT NULL,
  `developer_description` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_developer`
--

INSERT INTO `tbl_developer` (`developer_id`, `developer_name`, `developer_email_address`, `developer_description`, `publication_status`) VALUES
(1, 'Alex ye', 'asdf@ssdf.cpm', 'sfEfss', 1),
(2, 'shofin', 'shofin.cse@ghgk.com', 'well', 1),
(3, 'nora for (5)', 'asd@gmail.com', 'asdasfd', 1),
(4, 'habib ullah', 'ha@gmail.com', 'asfdefgr ger', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_flagship_models`
--

CREATE TABLE `tbl_flagship_models` (
  `flagship_id` int(4) NOT NULL,
  `flagship_title` varchar(100) NOT NULL,
  `flagship_model_name` varchar(100) NOT NULL,
  `short_description` text NOT NULL,
  `key_feature` text NOT NULL,
  `flagship_image` text NOT NULL,
  `flagship_video` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_flagship_models`
--

INSERT INTO `tbl_flagship_models` (`flagship_id`, `flagship_title`, `flagship_model_name`, `short_description`, `key_feature`, `flagship_image`, `flagship_video`, `publication_status`) VALUES
(1, 'Best Camera', 'Symphony ZVIII', 'Super AMOLED capacitive touchscreen, 16M colors,32/64 GB, 4 GB RAM, Fingerprint sensor', 'camera, Music, OTG, Power bank, 4900ma battery', '../assets/images/flagship/zviii-grey-all_img_4_246.png', '<iframe width="854" height="480" src="https://www.youtube.com/embed/LoTdpRnAjR8" frameborder="0" allowfullscreen></iframe>', 1),
(2, 'Smart Selfie', 'Symphony R20', 'Camera: 8 mp/5mp LED Flash, Face Beauty, Panorama, Smile Shot, HDR, up to 4x Zoom\r\nDisplay Size & Resolution	5.0 inches, HD 720 Ã— 1280 pixels (294 ppi), IPS Touchscreen\r\nMemory Card Slot- MicroSD, up to 64 GB\r\nOperating System	Android Marshmallow v6.0\r\nProcessor	Quad-Core, 1.3 GHz\r\nRAM/ROM 1GB/8GB\r\nDual SIM card (Micro,Mini)', '    Quad Core 1.3GHz Processor\r\n    Power Bank\r\n    5" QHD IPS Display\r\n   OTG Support\r\n    Android OS Lolipop\r\n    4900mAh Battery\r\n    Other Features	- Bluetooth, GPS, A-GPS, MP3, MP4, Radio, GPRS, Edge, Multitouch, Loudspeaker\r\n    Performance - 70%\r\n   Camera performance 70%\r\n', '../assets/images/flagship/Symphony R20-1100x800.jpg', '<iframe width="854" height="480" src="https://www.youtube.com/embed/mE_SHzI1plQ" frameborder="0" allowfullscreen></iframe>', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_knowledge_share`
--

CREATE TABLE `tbl_knowledge_share` (
  `knowledge_share_id` int(3) NOT NULL,
  `date_time` text NOT NULL,
  `knowledge_share_title` text NOT NULL,
  `publisher_name` text NOT NULL,
  `knowledge_share_contents` text NOT NULL,
  `knowledge_share_file` text NOT NULL,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_knowledge_share`
--

INSERT INTO `tbl_knowledge_share` (`knowledge_share_id`, `date_time`, `knowledge_share_title`, `publisher_name`, `knowledge_share_contents`, `knowledge_share_file`, `publication_status`) VALUES
(1, '2017-05-18 23:01:48', 'Automated Testing Tools', 'Md. Safiul Islam', 'https://developer.android.com/training/testing/ui-testing/uiautomator-testing.html', '', 1),
(2, '2017-05-18 23:58:29', 'Test your android apps', 'Md. safiul Islam', 'https://developer.android.com/studio/test/index.html', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_meetings`
--

CREATE TABLE `tbl_meetings` (
  `meetings_id` int(3) NOT NULL,
  `meeting_minutes_image` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `duration` varchar(30) NOT NULL,
  `chair` text NOT NULL,
  `subject` text NOT NULL,
  `short_description` text NOT NULL,
  `area_1` text NOT NULL,
  `decision_1` text NOT NULL,
  `responsible_1` text NOT NULL,
  `target_date_1` text NOT NULL,
  `area_2` text NOT NULL,
  `decision_2` text NOT NULL,
  `responsible_2` text NOT NULL,
  `target_date_2` text NOT NULL,
  `area_3` text NOT NULL,
  `decision_3` text NOT NULL,
  `responsible_3` text NOT NULL,
  `target_date_3` text NOT NULL,
  `remarks` text NOT NULL,
  `meeting_minutes_file` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_meetings`
--

INSERT INTO `tbl_meetings` (`meetings_id`, `meeting_minutes_image`, `date`, `duration`, `chair`, `subject`, `short_description`, `area_1`, `decision_1`, `responsible_1`, `target_date_1`, `area_2`, `decision_2`, `responsible_2`, `target_date_2`, `area_3`, `decision_3`, `responsible_3`, `target_date_3`, `remarks`, `meeting_minutes_file`, `publication_status`) VALUES
(1, '../assets/images/meeting/14796160_1212100102166730_2137720550_o.jpg', '2017-05-30 19:38:14', '12:00~1:30 1.5 hours', 'Munim Md. Istiaque', 'Weekly PMI meeting', 'Various SW and project related issues with testers and project owners had been discussed. Action points registered forreporting (After sales and Test case weekly) for operational smoothness. Additional responsibilities has been discussed and distributed.', 'Project Status', 'Project Status will be shared by Project owners & it will be uploaded to Google drive by Testers', 'ALL PO Members+ All testers', 'Next Meeting', 'Open issue status share', 'A report of open issue tracking and feedback and progress will be prepared.', 'Munir', 'On Progress', 'SW requirements Redefine', 'Necessary and unnecessary SW requirements add and delete from SW requirements file', 'Munir', '15th Marchâ€™17', 'SW major issue\r\nSW challenges ODM wise excel prepare & Major issue presentation. \r\nALL PO Members\r\nNext Meet', '../assets/images/meeting/testing_pdf_report_summary.pdf', 1),
(2, '../assets/images/meeting/14796160_1212100102166730_2137720550_o.jpg', '2017-05-26 01:34:54', '12:00~1:30 1.5 hours', 'Munim Md. Istiaque', 'Weekly PMI meeting 2', 'Various SW and project related issues with testers and project owners had been discussed. Action points registered forreporting (After sales and Test case weekly) for operational smoothness. Additional responsibilities has been discussed and distributed.', 'Project Status', 'Project Status will be shared by Project owners & it will be uploaded to Google drive by Testers', 'ALL PO Members+ All testers', 'Next Meeting', 'Open issue status share', 'A report of open issue tracking and feedback and progress will be prepared.', 'Munir', 'On Progress', 'SW requirements Redefine', 'Necessary and unnecessary SW requirements add and delete from SW requirements file', 'Munir', '15th Marchâ€™17', 'SW major issue\r\nSW challenges ODM wise excel prepare & Major issue presentation. \r\nALL PO Members\r\nNext Meet', '../assets/images/meeting/testing_pdf_report_summary.pdf', 0),
(3, '../assets/images/meeting/18237766_612323532302175_3862735244355532657_o.jpg', '2017-05-30 21:34:20', '0:0:2', 'asdf', 'assdasf efe', 'afcaavsdfa', '', '', '', '', '', '', '', '', '', '', '', '', '', '../assets/images/meeting/', 0),
(4, '../assets/images/meeting/18237766_612323532302175_3862735244355532657_o.jpg', '2017-06-01 00:40:17', '', 'Mr. Khiladi', 'demo', 'awdwef wefgefrert hrrrt htyhdtyhjty jt dy jdtyf', 'Project Status', 'sefwertwetwegsdfgsdg', 'ALL PO Members+ All testers', 'Next Meeting', 'Open issue status share', 'sdefrta4erg ert rhdrtdft gfwertg', '', '', 'SW requirements Redefine', 'uytyrtujkbvy78yoihk', '', '', 'iuguyhjbuyft9oih yuydt7gho ino', '../assets/images/meeting/', 0),
(5, '../assets/images/meeting/15747638_401809560160169_863602296457838652_n.jpg', '2017-06-01 00:45:40', '', 'Munim Md. Istiaque', 'SSSSS', 'lbvhjblk', '', '', '', '', '', '', '', '', '', '', '', '', '', '../assets/images/meeting/', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_model_info`
--

CREATE TABLE `tbl_model_info` (
  `model_id` int(5) NOT NULL,
  `model_name` varchar(100) NOT NULL,
  `phone_type` varchar(100) NOT NULL DEFAULT 'smart',
  `project_owner_id` int(25) NOT NULL,
  `odm_id` int(3) DEFAULT NULL,
  `spec_and_npd` varchar(100) DEFAULT NULL,
  `po_date` date DEFAULT NULL,
  `launching_year` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_model_info`
--

INSERT INTO `tbl_model_info` (`model_id`, `model_name`, `phone_type`, `project_owner_id`, `odm_id`, `spec_and_npd`, `po_date`, `launching_year`) VALUES
(1, 'E62', 'smart', 4, 6, 'http://redmine.edison-bd.com:3000/documents/207', '2016-10-19', 2017),
(2, 'V110', 'smart', 3, 7, 'http://redmine.edison-bd.com:3000/documents/208	\r\n', '2016-10-19', 2017),
(3, 'V65', 'smart', 3, 7, 'http://redmine.edison-bd.com:3000/documents/209', '2016-10-19', 2017),
(4, 'V34', 'smart', 2, 4, 'http://redmine.edison-bd.com:3000/documents/211', '2016-10-25', 2017),
(5, 'Z8', 'smart', 5, 4, 'http://redmine.edison-bd.com:3000/documents/212', '2016-10-25', 2017),
(6, 'i25', 'smart', 2, 8, 'http://redmine.edison-bd.com:3000/documents/215', '2016-10-30', 2017),
(7, 'HelioS25', 'smart', 5, 8, 'http://redmine.edison-bd.com:3000/documents/216', '2016-10-30', 2017),
(8, 'D91', 'feature', 4, 3, 'http://redmine.edison-bd.com:3000/documents/217', '2016-10-30', 2017),
(9, 'D105', 'feature', 1, 9, 'http://redmine.edison-bd.com:3000/documents/218', '2016-10-30', 2017),
(10, 'T200', 'feature', 1, 9, 'http://redmine.edison-bd.com:3000/documents/219', '2016-10-30', 2017),
(11, 'V47', 'smart', 3, 10, 'http://redmine.edison-bd.com:3000/documents/237', '2017-01-18', 2017),
(12, 'i21', 'smart', 2, 7, NULL, '2017-02-15', 2017),
(13, 'Z9', 'smart', 1, 2, 'http://redmine.edison-bd.com:3000/documents/239', '2017-03-05', 2017),
(14, 'T105', 'feature', 1, 3, 'http://redmine.edison-bd.com:3000/documents/240', '2017-03-05', 2017),
(15, 'i100', 'smart', 5, 11, NULL, '2017-03-02', 2017),
(16, 'P9', 'smart', 5, 11, NULL, '2017-03-02', 2017),
(17, 'V42', 'smart', 3, 7, NULL, '2017-03-19', 2017),
(18, 'T110', 'feature', 4, 12, NULL, '2017-03-14', 2017),
(19, 'L65', 'feature', 4, 12, NULL, '2017-03-02', 2017),
(20, 'B50', 'feature', 4, 12, NULL, '2017-03-02', 2017),
(21, 'D68', 'feature', 4, 10, NULL, '2017-03-02', 2017),
(22, 'B20', 'feature', 1, 1, NULL, '2017-03-14', 2017),
(23, 'D101', 'feature', 1, 1, NULL, '2017-03-14', 2017),
(24, 'P7Pr0', 'smart', 5, 2, NULL, '2016-11-28', 2017),
(25, 'SYMTab25', 'Tab', 2, 13, NULL, '2017-03-22', 2017),
(26, 'HelioS10', 'smart', 5, 8, NULL, '2017-03-20', 2017),
(27, 'E82', 'smart', 3, 7, '', '2017-05-11', 2017);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news_feed`
--

CREATE TABLE `tbl_news_feed` (
  `news_feed_id` int(5) NOT NULL,
  `news_date_time` text NOT NULL,
  `news_title` text NOT NULL,
  `news_contents` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_news_feed`
--

INSERT INTO `tbl_news_feed` (`news_feed_id`, `news_date_time`, `news_title`, `news_contents`, `publication_status`) VALUES
(2, '2017-05-18 22:14:43', 'Regarding Next Meeting', 'This is a simple reminder to you that our next PMI meeting will be held 10th July at 11:00 am ~ 12:30 pm.', 1),
(5, 'Fri May 26 2017 2:32:10', 'yyyy', 'eee', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_normal_issue`
--

CREATE TABLE `tbl_normal_issue` (
  `normal_issue_id` int(3) NOT NULL,
  `project_id` int(3) NOT NULL,
  `odm_id` int(3) NOT NULL,
  `chipset` varchar(15) NOT NULL,
  `title_of_issue` text NOT NULL,
  `issue_type` tinyint(1) NOT NULL,
  `platform` text NOT NULL,
  `remarks` text NOT NULL,
  `added_by` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_normal_issue`
--

INSERT INTO `tbl_normal_issue` (`normal_issue_id`, `project_id`, `odm_id`, `chipset`, `title_of_issue`, `issue_type`, `platform`, `remarks`, `added_by`) VALUES
(2, 5, 3, '678', 'well', 2, 'Marshmallow cc', 'well', 'Md. Safiul Islam'),
(4, 4, 4, '1254', 'huy', 2, 'kitkat', 'huhuhu', 'Md. Safiul Islam');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_odm`
--

CREATE TABLE `tbl_odm` (
  `odm_id` int(2) NOT NULL,
  `odm_name` text NOT NULL,
  `contact_1` text NOT NULL,
  `contact_2` text NOT NULL,
  `odm_description` text NOT NULL,
  `publication_status` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_odm`
--

INSERT INTO `tbl_odm` (`odm_id`, `odm_name`, `contact_1`, `contact_2`, `odm_description`, `publication_status`) VALUES
(1, 'Gomtel', 'Harry<harry.cheng@gomtel.com>', 'Alex	<qiang.wang@gomtel.com>', 'Van	<van.yu@gomtel.com>', 1),
(2, 'Dewav', 'jake.xiang@dewav.com', 'frederic.shi@dewav.com', '', 1),
(3, 'Huano', 'Grace wu <grace.wu@huanoint.com>', 'N/A', '', 1),
(4, 'Tinno', 'Felix <felix.li@tinno.com>', 'tony.zhou <tony.zhou@tinno.com>', '', 1),
(5, 'Ragentek', 'N/A', '', '', 1),
(6, 'FortuneShip 01', 'Gavin<hyongjiu@gm-mobile.com >', 'Vincent<vincent_wj@gm-mobile.com>', '', 1),
(7, 'Sporrocom', 'amanda.peng@sprocomm.com', 'leon.guo@sprocomm.com', '', 1),
(8, 'Gionee', 'Joyce <yinfei@gionee.com>', 'Vivian <zengling@gionee.com>', '', 1),
(9, 'Symsonic', 'Nancy <nancy@symsonic.cn>', 'Ye <ye@symsonic.cn>', '', 1),
(10, 'Fortuneship 5', 'Alex <ychao@uceemobile.com>', '', '', 1),
(11, 'Kozen', 'Claire Yang<claire.yang@kozenmobile.com>', 'Bello<bello.he@xchengtech.com>', '', 1),
(12, 'Mobiwire', 'Linda<Linda@mobiwire.com>', 'Paddy<Paddy.Ding@mobiwire.com', 'Paddy<xiaoxiao.ding@mobiwire.com>', 1),
(13, 'Miki', 'james', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pds`
--

CREATE TABLE `tbl_pds` (
  `pds_id` int(3) NOT NULL,
  `pds_files` text NOT NULL,
  `user_manual` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_pds`
--

INSERT INTO `tbl_pds` (`pds_id`, `pds_files`, `user_manual`) VALUES
(1, '../assets/images/user/checklist manage.txt', ''),
(2, '../assets/images/user/bd-business_Cpanel.txt', ''),
(3, '../assets/images/user/bd-business_Cpanel.txt', '../assets/images/user/checklist manage.txt'),
(4, '../assets/images/user/Employees Evaluation form with QA Justification_Safiul.xlsx', '../assets/images/user/team lead.txt'),
(5, '../assets/images/user/', '../assets/images/user/');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_project`
--

CREATE TABLE `tbl_project` (
  `project_id` int(3) NOT NULL,
  `project_name` text NOT NULL,
  `project_description` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_project`
--

INSERT INTO `tbl_project` (`project_id`, `project_name`, `project_description`, `publication_status`) VALUES
(1, 'r20', 'scvsdfc', 1),
(2, 'v120', 'well', 1),
(3, 'v65', 'kcbjsndkcn spojcpo', 1),
(4, 'v110', 'sdvdsvsdvdsvsdv', 1),
(5, 'v42', 'vsdfvdsvsd', 1),
(6, 'E-82', 'zscadvsg rg', 1),
(7, 'v20', 'asfaf sfs', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_project_owner`
--

CREATE TABLE `tbl_project_owner` (
  `project_owner_id` int(2) NOT NULL,
  `project_owner_name` text NOT NULL,
  `edison_id` int(5) NOT NULL,
  `email_address` varchar(30) NOT NULL,
  `project_owner_description` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_project_owner`
--

INSERT INTO `tbl_project_owner` (`project_owner_id`, `project_owner_name`, `edison_id`, `email_address`, `project_owner_description`, `publication_status`) VALUES
(1, 'Md. Munirul Hoque ', 1128, 'munirul.hoque@edison-bd.com', 'well', 1),
(2, 'Avijit Sarker', 1123, 'Avijit.sarkar@edison-bd.com', '', 1),
(3, 'Dil Afroz Farzana', 1223, 'dil.farzana@edison-bd.com', '', 1),
(4, 'Farhat Redwan', 1269, 'farhat.redwan@edison-bd.com', '', 1),
(5, 'Md. Rubaiyat Islam', 1104, 'md.rubaiyat@edison-bd.com', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_project_summary`
--

CREATE TABLE `tbl_project_summary` (
  `project_summary_id` int(3) NOT NULL,
  `project_id` int(3) NOT NULL,
  `project_type` tinyint(1) NOT NULL,
  `odm_id` int(3) NOT NULL,
  `developer_id` int(3) NOT NULL,
  `project_owner_id` int(3) NOT NULL,
  `tester1_id` int(2) NOT NULL DEFAULT '1',
  `tester2_id` int(2) NOT NULL DEFAULT '1',
  `tester3_id` int(2) NOT NULL DEFAULT '1',
  `tester4_id` int(2) NOT NULL DEFAULT '1',
  `hardware_version` int(1) NOT NULL,
  `current_soft_version` int(2) NOT NULL,
  `total_issue` int(5) NOT NULL,
  `resolve_issue` int(5) NOT NULL,
  `open_issue` int(3) NOT NULL,
  `normal_issue` int(3) NOT NULL,
  `after_sales_issue` int(3) NOT NULL,
  `after_sales_resolve_issue` int(5) NOT NULL,
  `confirm_soft_version` text NOT NULL,
  `confirmation_date` date NOT NULL,
  `shipment_date` date NOT NULL,
  `any_difficulties` text NOT NULL,
  `models_image` text NOT NULL,
  `final_pds` text NOT NULL,
  `publication_status` int(1) NOT NULL,
  `product_category` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_project_summary`
--

INSERT INTO `tbl_project_summary` (`project_summary_id`, `project_id`, `project_type`, `odm_id`, `developer_id`, `project_owner_id`, `tester1_id`, `tester2_id`, `tester3_id`, `tester4_id`, `hardware_version`, `current_soft_version`, `total_issue`, `resolve_issue`, `open_issue`, `normal_issue`, `after_sales_issue`, `after_sales_resolve_issue`, `confirm_soft_version`, `confirmation_date`, `shipment_date`, `any_difficulties`, `models_image`, `final_pds`, `publication_status`, `product_category`) VALUES
(2, 1, 1, 4, 4, 3, 1, 1, 1, 1, 1, 4, 100, 95, 3, 2, 0, 0, '2', '0000-00-00', '0000-00-00', '', '../assets/images/project/1234.jpg', '../assets/images/project/Employees Evaluation form with QA Justification_Safiul.xlsx', 1, 'A'),
(3, 3, 1, 2, 3, 3, 6, 18, 14, 13, 2, 6, 0, 0, 0, 0, 0, 0, '0', '0000-00-00', '0000-00-00', 'induction and optics in an attempt to solve one of the holy grails of physics, gravitational-waves, I could not have been more pleased. Thus vindicated, my desire to further formalize my love of science brings me to State University. Thanks to this experience, I know now better than ever that State University is my future, because through it I seek another, permanent, opportunity to follow my passion for science and engineering.  In addition to just science, I am drawn to State University for other reasons. I strive to work with the diverse group of people that State University wholeheartedly accommodates – and who also share my mindset. They, like me, are there because State University respects the value of diversity. I know from personal experience that in order to achieve the trust, honesty, and success that State University values, new people are needed to create a respectful environment for these values. I feel that my background as an American Sikh will provide an innovative perspective in the university’s search for knowledge while helping it to develop a basis for future success. And that, truly, is the greatest success I can imagine', '../assets/images/project/product - Copy.jpg', '../assets/images/project/2.pdf', 1, 'B'),
(4, 4, 1, 3, 3, 6, 8, 1, 16, 1, 1, 5, 200, 152, 45, 3, 1, 1, '', '2017-04-25', '2018-05-06', 'ytedxckhjvikkl', '../assets/images/project/IMG_6468_edit.jpg', '../assets/images/project/', 1, 'B');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `slider_id` int(2) NOT NULL,
  `slider_name` varchar(50) NOT NULL,
  `slider_title` varchar(100) NOT NULL,
  `slider_description` text NOT NULL,
  `slider_image` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`slider_id`, `slider_name`, `slider_title`, `slider_description`, `slider_image`, `publication_status`) VALUES
(2, 'Edison Futsal', 'FIRE Ball', 'You are here we are glad. cheer up guys. ', '../assets/images/main_slider/IMG_1438-min.jpg', 1),
(7, 'Team Fireball', 'Play to enjoy not only to win :)', 'We love to play. We love to fun. We love to give our best.', '../assets/images/main_slider/IMG_2477.jpg', 1),
(8, 'à¦à¦¬à¦¾à¦°à§‡à¦° à¦¬à§ˆà¦¶à¦¾à¦– -à§§à§ªà§¨à§ª ', 'à¦¬à§ˆà¦¶à¦¾à¦– à¦†à¦®à¦¾à¦° à¦à¦¤à¦¿à¦¹à§à¦¯', 'à¦¦à¦¿à¦¨ à¦¶à§‡à¦·à§‡ à¦†à¦®à¦°à¦¾ à¦¸à¦¬à¦¾à¦‡ à¦¬à¦¾à¦™à¦¾à¦²à§€à¥¤ à¦¬à§ˆà¦¶à¦¾à¦–à§‡à¦° à¦†à¦¨à¦¨à§à¦¦ à¦›à§à¦à§Ÿà§‡ à¦¯à¦¾à¦• à¦ªà§à¦°à¦¤à¦¿à¦Ÿà¦¿ à¦…à¦¨à§à¦¤à¦°à§‡à¥¤ à¦®à§à¦›à§‡ à¦¯à¦¾à¦• à¦—à§à¦²à¦¾à¦¨à§€ à¦¶à§à¦°à§ à¦¹à§‹à¦• à¦¨à¦¤à§à¦¨ à¦ªà¦¥à¦šà¦²à¦¾à¥¤ ', '../assets/images/main_slider/IMG_3882.jpg', 1),
(9, 'testting ', 'test to update ', 'Its working......', '../assets/images/main_slider/15391277_16355593698_jItbG.jpg', 0),
(11, 'Team ', 'Null Pointer', 'You are here, we are glad.', '../assets/images/main_slider/IMG_4014_slider.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_team_leader`
--

CREATE TABLE `tbl_team_leader` (
  `team_leader_id` int(5) NOT NULL,
  `team_leader_name` text NOT NULL,
  `nick_name` text NOT NULL,
  `team_leader_designation` text NOT NULL,
  `employe_id` int(5) NOT NULL,
  `career_short_description` text NOT NULL,
  `team_leader_image` text NOT NULL,
  `publication_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_team_leader`
--

INSERT INTO `tbl_team_leader` (`team_leader_id`, `team_leader_name`, `nick_name`, `team_leader_designation`, `employe_id`, `career_short_description`, `team_leader_image`, `publication_status`) VALUES
(1, 'Md. Safiul Islam', 'Shofin', 'Executive Engineer', 1319, 'Former Junior Engineer in Samsung R&D Institute Bangladesh (SRBD).\r\nFormer Documentation Engineer in Tusuka Technotrade Limited.\r\nBSc in Computer Science & Engineering form Dhaka International University, Dhaka.\r\nDiploma in Computer Engineering from Bogra Polytechnic Institute,Bogra.', '../assets/images/team_leader/Safiul_05401.jpg', 0),
(3, 'Raja Hassan', 'Apple', 'Executive Engineer', 1333, 'asdfdhwep9df hnwwiuefhwo;fh8werhriogj', '../assets/images/team_leader/IMG_6468_edit1111.jpg', 0),
(4, 'Tasmiha Tajkira ', 'Neepa (Dushman)', 'Executive Engineer', 1334, 'well', '../assets/images/team_leader/IMG_4126_11.jpg', 0),
(5, 'Dil Afroz Farzana', 'Jenny', 'senior Executive', 1223, 'Former Senior Software Engineer at Samsung R&D Institute (SRBD)', '../assets/images/team_leader/IMG_4020.jpg', 1),
(6, 'Md. Munirul Hoque', '', 'Manager', 1128, 'Former Software Engineer at Samsung R&D Institute Bangladesh (SRBD)', '../assets/images/team_leader/IMG_3900.jpg', 1),
(7, 'Munim Md. Istiaque', '', 'Additional General Manager', 14, 'Head Of Product Management & Quality Control.', '../assets/images/team_leader/best.jpg', 1),
(8, 'Farhat Redwan', '', 'Assistant Manager', 1269, 'Well', '../assets/images/team_leader/farhat vai.jpg', 1),
(9, 'Md. Aminur Rashid', '', 'Chairman and CEO', 1, 'Founder Of Edison Group.', '../assets/images/team_leader/chairman.jpg', 0),
(10, 'Sazia Sharmin ', 'Sneha (Dadu)', 'Executive Engineer', 1318, 'well', '../assets/images/team_leader/sneha.jpg', 0),
(11, 'Mitu Ishrat', 'Mitu', 'Executive Engineer', 1467, 'well', '../assets/images/team_leader/mitu.jpg', 0),
(13, 'Sifat Jannat', 'Tuktuki', 'Executive Engineer', 1550, 'well', '../assets/images/team_leader/jannat.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tester`
--

CREATE TABLE `tbl_tester` (
  `tester_id` int(2) NOT NULL,
  `tester_name` text NOT NULL,
  `tester_email_address` varchar(30) NOT NULL,
  `tester_description` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_tester`
--

INSERT INTO `tbl_tester` (`tester_id`, `tester_name`, `tester_email_address`, `tester_description`, `publication_status`) VALUES
(1, 'Not-Assaign', 'safi.cse@edison-bd.com', 'well', 1),
(6, 'Md. Safiul Islam', 'safiul.islam@edison-bd.com', 'well', 1),
(7, 'Md. Sharifuzzaman', 'sharifuzzaman@edison-bd.com', 'well', 1),
(8, 'Md. Najmul Islam', 'najmul.islam@edison-bd.com', 'well', 1),
(9, 'Mitu Ishrat', 'mitu.ishrat@edison-bd.com', 'well', 1),
(10, 'Sifatul Jannat', 'sifatul.jannat@edison-bd.com', 'well', 1),
(11, 'Md. Ainul Huda', 'md.ainul@edison-bd.com', 'well', 1),
(12, 'Md. Harun- Ar- Rashid', 'm.harun@edison-bd.com', 'well', 1),
(13, 'Sazia Sharmin Ahmed Sneha', 'sazia.sneha@edison-bd.com', 'well', 1),
(14, 'Shihab Z Hasan', 'shihab.hasan@edison-bd.com', 'well', 1),
(15, 'Md. Sharif Bhuiyan', 'sharif.bhuiyan@edison-bd.com', 'well', 1),
(16, 'Md. Rana Hossai', 'mrana.hossain@edison-bd.com', 'well', 1),
(17, 'Md. Raja Hassan', 'raja.hassan@edison-bd.com', 'well', 1),
(18, 'Tasmiha Tajkira', 'tasmiha.tajkira@edison-bd.com', 'well', 1),
(19, 'Khaled Al Maroof', 'khaled.maroof@edison-bd.com', 'well', 1),
(20, 'Md. Jewel Rana', 'md.jewel@edison-bd.com', 'well', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(4) NOT NULL,
  `user_name` varchar(40) NOT NULL,
  `employe_id` int(6) NOT NULL,
  `phone_number` int(15) NOT NULL,
  `email_address` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `pin_number` int(4) NOT NULL,
  `career_short_description` text NOT NULL,
  `user_image` text NOT NULL,
  `approve_status` tinyint(1) NOT NULL,
  `pm_member` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_name`, `employe_id`, `phone_number`, `email_address`, `password`, `pin_number`, `career_short_description`, `user_image`, `approve_status`, `pm_member`) VALUES
(11, 'Safiul Islam', 1319, 1737153148, 'user@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 123456, 'junior eng. at Samsung r&d institute bangladesh. former junior enginer at tusuka.com', 'assets/images/user/safiul_1.jpg', 1, 1),
(21, 'Nazim', 1234, 1234, 'nazim@edison-bd.com', 'e10adc3949ba59abbe56e057f20f883e', 111111, 'junior eng. at Samsung r&d institute bangladesh', 'assets/images/user/pms.jpg', 1, 0),
(22, 'Sneha', 1318, 6513188, 'sneha@edison-bd.com', 'e10adc3949ba59abbe56e057f20f883e', 123456, 'Executive at edison group', 'assets/images/user/sneha.jpg', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_about_us`
--
ALTER TABLE `tbl_about_us`
  ADD PRIMARY KEY (`about_us_id`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_assets`
--
ALTER TABLE `tbl_assets`
  ADD PRIMARY KEY (`asset_id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tbl_common_issue`
--
ALTER TABLE `tbl_common_issue`
  ADD PRIMARY KEY (`common_issue_id`);

--
-- Indexes for table `tbl_developer`
--
ALTER TABLE `tbl_developer`
  ADD PRIMARY KEY (`developer_id`);

--
-- Indexes for table `tbl_flagship_models`
--
ALTER TABLE `tbl_flagship_models`
  ADD PRIMARY KEY (`flagship_id`);

--
-- Indexes for table `tbl_knowledge_share`
--
ALTER TABLE `tbl_knowledge_share`
  ADD PRIMARY KEY (`knowledge_share_id`);

--
-- Indexes for table `tbl_meetings`
--
ALTER TABLE `tbl_meetings`
  ADD PRIMARY KEY (`meetings_id`);

--
-- Indexes for table `tbl_model_info`
--
ALTER TABLE `tbl_model_info`
  ADD PRIMARY KEY (`model_id`);

--
-- Indexes for table `tbl_news_feed`
--
ALTER TABLE `tbl_news_feed`
  ADD PRIMARY KEY (`news_feed_id`);

--
-- Indexes for table `tbl_normal_issue`
--
ALTER TABLE `tbl_normal_issue`
  ADD PRIMARY KEY (`normal_issue_id`);

--
-- Indexes for table `tbl_odm`
--
ALTER TABLE `tbl_odm`
  ADD PRIMARY KEY (`odm_id`);

--
-- Indexes for table `tbl_pds`
--
ALTER TABLE `tbl_pds`
  ADD PRIMARY KEY (`pds_id`);

--
-- Indexes for table `tbl_project`
--
ALTER TABLE `tbl_project`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `tbl_project_owner`
--
ALTER TABLE `tbl_project_owner`
  ADD PRIMARY KEY (`project_owner_id`);

--
-- Indexes for table `tbl_project_summary`
--
ALTER TABLE `tbl_project_summary`
  ADD PRIMARY KEY (`project_summary_id`);

--
-- Indexes for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `tbl_team_leader`
--
ALTER TABLE `tbl_team_leader`
  ADD PRIMARY KEY (`team_leader_id`),
  ADD UNIQUE KEY `employe_id` (`employe_id`);

--
-- Indexes for table `tbl_tester`
--
ALTER TABLE `tbl_tester`
  ADD PRIMARY KEY (`tester_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `employe_id` (`employe_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_about_us`
--
ALTER TABLE `tbl_about_us`
  MODIFY `about_us_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_assets`
--
ALTER TABLE `tbl_assets`
  MODIFY `asset_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `category_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_common_issue`
--
ALTER TABLE `tbl_common_issue`
  MODIFY `common_issue_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_developer`
--
ALTER TABLE `tbl_developer`
  MODIFY `developer_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_flagship_models`
--
ALTER TABLE `tbl_flagship_models`
  MODIFY `flagship_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_knowledge_share`
--
ALTER TABLE `tbl_knowledge_share`
  MODIFY `knowledge_share_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_meetings`
--
ALTER TABLE `tbl_meetings`
  MODIFY `meetings_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_model_info`
--
ALTER TABLE `tbl_model_info`
  MODIFY `model_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `tbl_news_feed`
--
ALTER TABLE `tbl_news_feed`
  MODIFY `news_feed_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_normal_issue`
--
ALTER TABLE `tbl_normal_issue`
  MODIFY `normal_issue_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_odm`
--
ALTER TABLE `tbl_odm`
  MODIFY `odm_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_pds`
--
ALTER TABLE `tbl_pds`
  MODIFY `pds_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_project`
--
ALTER TABLE `tbl_project`
  MODIFY `project_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_project_owner`
--
ALTER TABLE `tbl_project_owner`
  MODIFY `project_owner_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_project_summary`
--
ALTER TABLE `tbl_project_summary`
  MODIFY `project_summary_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  MODIFY `slider_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tbl_team_leader`
--
ALTER TABLE `tbl_team_leader`
  MODIFY `team_leader_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_tester`
--
ALTER TABLE `tbl_tester`
  MODIFY `tester_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
