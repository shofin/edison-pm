<?php
require 'classes/login_signup.php';

$obj_login_signup = new Login_signup();
$message_admin = '';
if (isset($_POST['btn'])) {

    $to= $_POST['email_address'];
    $subject= "sign up edison-pm";
    $message= "This is an automated mail.Do not reply.";
    
    mail($to, $subject, $message);
    $message_admin = $obj_login_signup->new_user_info($_POST);
    
}
?>


<html>
    <head>
        <title>Sign Up Form</title>
        <!-- Meta tag Keywords -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Flat Sign Up Form Responsive Widget Template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- Meta tag Keywords -->
        <!-- css files -->
        <link href="assets/frontend/sign_up/css/style.css" rel="stylesheet" type="text/css" media="all">
        <link href="assets/frontend/sign_up/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
        <!-- //css files -->
        <!-- online-fonts -->
        <link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'><link href='//fonts.googleapis.com/css?family=Raleway+Dots' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <!--header-->
        <div class="header-w3l">
            <h1>Product Management Team</h1>
        </div>
        <!--//header-->
        <!--main-->
        <div class="main-agileits">
            <h3  style="text-align: center; color: white; font-size: 15px; padding-top: 10px;"><?php echo $message_admin; ?></h3>
            <h3  style="text-align: center; color: red"><?php
                if (isset($_SESSION['message'])) {
                    echo $_SESSION['message'];
                    unset($_SESSION['message']);
                }
                ?></h3>
            <h2 class="sub-head">Sign Up</h2>
            <div class="sub-main">	
                <form action="" method="post" enctype="multipart/form-data">
                    <input placeholder="Your Name" name="user_name" class="name" type="text" required="">
                    <span class="icon1"><i class="fa fa-user" aria-hidden="true"></i></span><br>

                    <input placeholder="Employe ID" name="employe_id" class="number" type="number" >
                    <span class="icon2"><i class="fa fa-user" aria-hidden="true"></i></span><br>

                    <input placeholder="Phone Number" name="phone_number" class="number" type="number" >
                    <span class="icon3"><i class="fa fa-phone" aria-hidden="true"></i></span><br>
                    <input placeholder="Email Address" name="email_address" class="email" type="email" required="">
                    <span class="icon4"><i class="fa fa-envelope" aria-hidden="true"></i></span><br>
                    <input  placeholder="Password" name="password" class="pass" type="password" >

                    <span class="icon5"><i class="fa fa-unlock" aria-hidden="true"></i></span><br>
                    <input  placeholder="Confirm Password" name="confirm_password" class="pass" type="password">
                    <span class="icon6"><i class="fa fa-unlock" aria-hidden="true"></i></span><br>

                    <input  placeholder="Security PIN Number" name="pin_number" class="pass" type="password" >
                    <span class="icon7"><i class="fa fa-lock" aria-hidden="true"></i></span><br>

                    <textarea name="career_short_description" placeholder="Career Short Description" class="form-group" cols="40" rows="6"></textarea>
<!--                    <span class="icon1"><i class="fa fa-user" aria-hidden="true"></i></span><br>-->

                    <div class="col-lg-9" style="color: #ff00ff">
                        Select Your Image
                        <input type="file"  name="user_image" > 
                    </div>
                    <input type="submit" name="btn" value="sign up">
                </form>
            </div>
            <div class="clear"></div>
        </div>
        <!--//main-->

        <!--footer-->
        <div class="footer-w3">
            <p>&copy; 2017 Edison-PM Form . All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>
        </div>
        <!--//footer-->

    </body>
</html>