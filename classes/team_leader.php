<?php

require_once 'db_connect.php';

class Team_leader extends Db_connect {

    public $link;

    public function __construct() {
        $this->link = $this->database_connection();
    }

    public function save_team_leader_info($data) {
        extract($data);
        $directory = '../assets/images/team_leader/';
        $target_file = $directory . $_FILES['team_leader_image']['name'];
        $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
        $file_size = $_FILES['team_leader_image']['size'];
        $check = getimagesize($_FILES['team_leader_image']['tmp_name']);
        if ($check) {
            if (file_exists($target_file)) {
                die("This file already uploaded. plese try one another");
            } else {
                if ($file_type != 'jpg' && $file_type != 'png') {
                    die('The file type is not valid. Please use jpg or png');
                } else {

                    if ($file_size > 50000000000000000) {
                        die("File size is too large. use with in 500mb");
                    } else {
                        move_uploaded_file($_FILES['team_leader_image']['tmp_name'], $target_file);
                        $sql = "INSERT INTO tbl_team_leader (team_leader_name,nick_name, team_leader_designation,employe_id,career_short_description,team_leader_image, publication_status) VALUES ('$team_leader_name','$nick_name', '$team_leader_designation','$employe_id','$career_short_description','$target_file', '$publication_status' )";
                        if (mysqli_query($this->link, $sql)) {
                            $message = "Team Leader info save successfully";
                            return $message;
                        } else {
                            die('Query problem' . mysqli_error($this->link));
                        }
                    }
                }
            }
        } else {
            die('The file you upload is not image. Plese use valid image file');
        }
    }

    public function select_all_published_team_leader_info() {
        $sql = "SELECT * FROM tbl_team_leader WHERE publication_status = 1";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function select_all_team_leader_info() {
        $sql = "SELECT * FROM tbl_team_leader";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function unpublished_team_leader_info_by_id($team_leader_id) {
        $sql = "UPDATE tbl_team_leader SET publication_status = 0 WHERE team_leader_id = '$team_leader_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'Team leader info unpublished successfully';
            return $message;
            // header('Loication: manage_category.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function published_team_leader_info_by_id($team_leader_id) {
        $sql = "UPDATE tbl_team_leader SET publication_status = 1 WHERE team_leader_id = '$team_leader_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'Team Leader info published successfully';
            return $message;
            //header('Loication: manage_category.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function select_team_leader_info_by_id($team_leader_id) {
        $sql = "SELECT * FROM tbl_team_leader WHERE team_leader_id = '$team_leader_id' ";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function update_team_leader_info_by_id($_data) {
        extract($_data);
        $team_leader_id = $_GET['id'];

        $new_image = $_FILES['team_leader_image']['name'];
        if ($new_image) {
            $directory = '../assets/images/team_leader/';
            $target_file = $directory . $_FILES['team_leader_image']['name'];
            $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
            $file_size = $_FILES['team_leader_image']['size'];
            $check = getimagesize($_FILES['team_leader_image']['tmp_name']);
            if ($check) {
                if (file_exists($target_file)) {
                    die("This file already uploaded. plese try one another");
                } else {
                    if ($file_type != 'jpg' && $file_type != 'png') {
                        die('The file type is not valid. Please use jpg or png');
                    } else {

                        if ($file_size > 5000000000000) {
                            die("File size is too large. use with in 500mb");
                        } else {
                            move_uploaded_file($_FILES['team_leader_image']['tmp_name'], $target_file);
                            $sql = "UPDATE tbl_team_leader SET team_leader_name= '$team_leader_name', nick_name='$nick_name',team_leader_designation='$team_leader_designation',employe_id='$employe_id',career_short_description='$career_short_description',team_leader_image='$target_file', publication_status= '$publication_status' WHERE team_leader_id = '$team_leader_id'";

                            if (mysqli_query($this->link, $sql)) {
                                $_SESSION['message'] = "Team Leader Info Update Successfully";
                                header('Location: manage_team_leader.php');
                            } else {
                                die('Query problem' . mysqli_error($this->link));
                            }
                        }
                    }
                }
            } else {
                die('The file you upload is not image. Plese use valid image file');
            }
        } else {

            $sql = "UPDATE tbl_team_leader SET team_leader_name= '$team_leader_name', nick_name='$nick_name',team_leader_designation='$team_leader_designation',employe_id='$employe_id',career_short_description='$career_short_description', publication_status= '$publication_status' WHERE team_leader_id = '$team_leader_id'";
            if (mysqli_query($this->link, $sql)) {
                $_SESSION['message'] = "Team Leader Info Update Successfully";
                header('Location: manage_team_leader.php');
            } else {
                die('Query problem' . mysqli_error($this->link));
            }
        }
    }

    public function delete_team_leader_info_by_id($team_leader_id) {
        $sql = "DELETE FROM tbl_team_leader WHERE team_leader_id = '$team_leader_id'";
        if (mysqli_query($this->link, $sql)) {
            $_SESSION['message'] = 'Team Leader info delete successfully';
            header('Location: manage_team_leader.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

}
