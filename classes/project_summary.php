<?php

require_once 'db_connect.php';

class Project_summary extends Db_connect {

    public $link;

    public function __construct() {
        $this->link = $this->database_connection();
    }

    public function save_project_summary_info($data) {
        extract($data);
        $directory = '../assets/images/project/';
        $target_file = $directory . $_FILES['models_image']['name'];
        $final_pds = $directory . $_FILES['final_pds']['name'];
        $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
        $file_size = $_FILES['models_image']['size'];
        $check = getimagesize($_FILES['models_image']['tmp_name']);
        if ($check) {
            if (file_exists($target_file)) {
                die("This file already uploaded. plese try one another");
            } else {
                if ($file_type != 'jpg' && $file_type != 'png') {
                    die('The file type is not valid. Please use jpg or png');
                } else {

                    if ($file_size > 500000000) {
                        die("File size is too large. use with in 5mb");
                    } else {
                        move_uploaded_file($_FILES['models_image']['tmp_name'], $target_file);
                        move_uploaded_file($_FILES['final_pds']['tmp_name'], $final_pds);
                        $sql = "INSERT INTO tbl_project_summary (project_id,project_type, odm_id,developer_id, project_owner_id,tester1_id,tester2_id,tester3_id,tester4_id,hardware_version,current_soft_version,total_issue,resolve_issue,open_issue,normal_issue,after_sales_issue,after_sales_resolve_issue,confirm_soft_version,confirmation_date,shipment_date,any_difficulties,models_image,final_pds,publication_status) VALUES ('$project_id','$project_type', '$odm_id','$developer_id', '$project_owner_id','$tester1_id','$tester2_id','$tester3_id','$tester4_id','$hardware_version','$current_soft_version','$total_issue','$resolve_issue', '$open_issue','$normal_issue', '$after_sales_issue', '$after_sales_resolve_issue', '$confirm_soft_version', '$confirmation_date','$shipment_date', '$any_difficulties', '$target_file','$final_pds', '$publication_status' )";
                    if (mysqli_query($this->link, $sql)) {
                            $message = "Isert Successfully";
                            return $message;
                        } else {
                            die('Query problem' . mysqli_error($this->link));
                        }
                    }
                }
            }
        } else {
            die('The file you upload is not image. Plese use valid image file');
        }
    }

    public function select_all_published_project_summary_info() {
        $sql = "SELECT s.project_summary_id,p.project_name,o.odm_name,d.developer_name,po.project_owner_name,s.total_issue,s.resolve_issue,s.confirm_soft_version,s.confirmation_date,s.shipment_date,s.publication_status FROM tbl_project_summary as s, tbl_project as p, tbl_odm as o, tbl_developer as d, tbl_project_owner as po WHERE s.project_id= p.project_id AND s.odm_id= o.odm_id AND s.developer_id=d.developer_id AND s.project_owner_id=po.project_owner_id ";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    
    public function details_project_summary_info_by_id($project_summary_id) {
//        $sql = "SELECT s.*,p.project_name,o.odm_name,d.developer_name,po.project_owner_name, t.tester_name FROM tbl_project_summary as s, tbl_project as p, tbl_odm as o, tbl_developer as d, tbl_project_owner as po,tbl_tester as t WHERE s.project_id= p.project_id AND s.odm_id= o.odm_id AND s.developer_id=d.developer_id AND s.project_owner_id=po.project_owner_id AND s.project_summary_id='$project_summary_id'";
        $sql="SELECT ps.*,project_summary_id,p.project_name,o.odm_name,d.developer_name,project_owner_name, t1.tester_name as tester_1_name,t2.tester_name as tester_2_name,t3.tester_name as tester_3_name,t4.tester_name as tester_4_name FROM tbl_project_summary as ps 
	 INNER JOIN tbl_project p ON p.project_id = ps.project_id
	 INNER JOIN tbl_odm o ON o.odm_id = ps.odm_id
	 INNER JOIN tbl_developer d ON d.developer_id = ps.developer_id
	 INNER JOIN tbl_project_owner po ON po.project_owner_id = ps.project_owner_id
         
INNER JOIN tbl_tester t1 ON t1.tester_id = ps.tester1_id
INNER JOIN tbl_tester t2 ON t2.tester_id = ps.tester2_id
INNER JOIN tbl_tester t3 ON t3.tester_id = ps.tester3_id 
INNER JOIN tbl_tester t4 ON t4.tester_id = ps.tester4_id AND ps.project_summary_id='$project_summary_id'
";
        
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function unpublished_project_summary_info_by_id($project_summary_id) {
        $sql = "UPDATE tbl_project_summary SET publication_status = 0 WHERE project_summary_id = '$project_summary_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'Project info unpublished successfully';
            return $message;
            // header('Loication: manage_category.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function published_project_summary_info_by_id($project_summary_id) {
        $sql = "UPDATE tbl_project_summary SET publication_status = 1 WHERE project_summary_id = '$project_summary_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'Project info published successfully';
            return $message;
            // header('Loication: manage_category.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function delete_project_summary_info_by_id($project_id) {
        $sql = "DELETE FROM tbl_project_summary WHERE project_id = '$project_id'";
        if (mysqli_query($this->link, $sql)) {
            $_SESSION['message'] = 'Flagship info delete successfully';
            header('Location: manage_project_summary.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    
    public function select_all_published_project_image() {
        $sql = "SELECT models_image,product_category FROM tbl_project_summary WHERE publication_status=1";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
            
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

}
