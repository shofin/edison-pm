<?php

class Normal_issue extends Db_connect {
    public $link;

    public function __construct() {
        $this->link = $this->database_connection();
    }
    public function save_normal_issue_info($data) {
        $added_by=$_SESSION['admin_name'];
        extract($data);
        $sql="INSERT INTO tbl_normal_issue (project_id, odm_id,chipset,title_of_issue,issue_type,platform, remarks,added_by) VALUES ('$project_id', '$odm_id','$chipset','$title_of_issue','$issue_type','$platform','$remarks', '$added_by' )";
        if(mysqli_query($this->link, $sql)) {
            $message="Normal Issue info save successfully";
            return $message;
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    
    public function select_all_normal_issue() {
        
        $sql="SELECT n.*,p.project_name,o.odm_name FROM tbl_normal_issue as n INNER JOIN tbl_project p ON p.project_id=n.project_id INNER JOIN tbl_odm o ON o.odm_id=n.odm_id";
        if(mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    public function select_normal_issue_by_id($normal_issue_id) {
        
        $sql="SELECT n.*,p.project_name,o.odm_name FROM tbl_normal_issue as n INNER JOIN tbl_project p ON p.project_id=n.project_id INNER JOIN tbl_odm o ON o.odm_id=n.odm_id WHERE n.normal_issue_id='$normal_issue_id'";
        if(mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    public function update_normal_issue_by_id($data) {
        extract($data);
        $sql="UPDATE tbl_normal_issue SET project_id = '$project_id', odm_id = '$odm_id', chipset = '$chipset',title_of_issue='$title_of_issue',issue_type='$issue_type',platform='$platform',remarks='$remarks' WHERE normal_issue_id = '$normal_issue_id' ";
        if(mysqli_query($this->link, $sql)) {
            $_SESSION['message']='Normal issue update successfully';
            header('Location: manage_normal_issue.php');
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    public function delete_normal_issue_by_id($normal_issue_id) {
         
        $sql="DELETE FROM tbl_normal_issue WHERE normal_issue_id = '$normal_issue_id' ";
        if(mysqli_query($this->link, $sql)) {
            $_SESSION['delete']=' Info delete successfully';
            header('Location: manage_normal_issue.php');
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    public function search_normal_issue($search_q) {
        
        $sql="SELECT n.*,p.project_name,o.odm_name FROM tbl_normal_issue as n INNER JOIN tbl_project p ON p.project_id=n.project_id INNER JOIN tbl_odm o ON o.odm_id=n.odm_id WHERE n.chipset LIKE '%$search_q%' ";
        if(mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
}
