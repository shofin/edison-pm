<?php
require_once 'db_connect.php';

class User extends Db_connect{
    public $link;

    public function __construct() {
        $this->link = $this->database_connection();
    }
    
    public function save_add_user_info($data) {
        extract($data);

        $password = md5($password);
        if ($data['password'] == $data['confirm_password']) {

            $directory = '../assets/images/user/';
            $target_file = $directory . $_FILES['user_image']['name'];
            $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
            $file_size = $_FILES['user_image']['size'];
            $check = getimagesize($_FILES['user_image']['tmp_name']);
            if ($check) {
                if (file_exists($target_file)) {
                    die("This file already uploaded. plese try one another");
                } else {
                    if ($file_type != 'jpg' && $file_type != 'png') {
                        die('The file type is not valid. Please use jpg or png');
                    } else {

                        if ($file_size > 500000000000000000) {
                            die("File size is too large. use with in 500mb");
                        } else {
                            move_uploaded_file($_FILES['user_image']['tmp_name'], $target_file);
                            $sql = "INSERT INTO tbl_user (user_name, employe_id, phone_number,email_address,password,pin_number,career_short_description,user_image) VALUES ('$user_name', '$employe_id', '$phone_number','$email_address','$password','$pin_number','$career_short_description', '$target_file' )";
                            if (mysqli_query($this->link, $sql)) {
                                $message = "Please wait untill approved.A confirmation maill will be send after verification";
                                return $message;
                            } else {
                                die('Query problem' . mysqli_error($this->link));
                            }
                        }
                    }
                }
            } else {
                die('The file you upload is not image. Plese use valid image file');
            }
        } else {
            echo 'Password Not Match. please Try again';
        }
    }
    
    public function select_all_user_info() {
        $sql = "SELECT * FROM tbl_user";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    
     public function not_approve_user_info_by_id($user_id) {
        $sql = "UPDATE tbl_user SET approve_status = 0 WHERE user_id = '$user_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'User info Reject successfully';
            return $message;
            // header('Loication: manage_category.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    
    public function approve_user_info_by_id($user_id) {
        $sql = "UPDATE tbl_user SET approve_status = 1 WHERE user_id = '$user_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'User info Approve successfully';
            return $message;
            // header('Loication: manage_category.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    
    public function set_normal_user_info_by_id($user_id) {
        $sql = "UPDATE tbl_user SET pm_member = 0 WHERE user_id = '$user_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'Set normal user successfully';
            return $message;
            // header('Loication: manage_category.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    
    public function set_pm_member_info_by_id($user_id) {
        $sql = "UPDATE tbl_user SET pm_member = 1 WHERE user_id = '$user_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'Set PM Member successfully';
            return $message;
            // header('Loication: manage_category.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
        
    public function delete_user_info_by_id($user_id) {
         $sql="DELETE FROM tbl_user WHERE user_id = '$user_id'";
        if(mysqli_query($this->link, $sql)) {
            $_SESSION['message']='User info delete successfully';
            header('Location: manage_user.php');
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
}
