<?php

class Project_owner extends Db_Connect {
    public $link;
    public function __construct() {
        $this->link=$this->database_connection();
    }
    public function save_project_owner_info($data) {
        extract($data);
        $sql="INSERT INTO tbl_project_owner (project_owner_name,edison_id,email_address, project_owner_description, publication_status) VALUES ('$project_owner_name','$edison_id','$email_address', '$project_owner_description', '$publication_status' )";
        if(mysqli_query($this->link, $sql)) {
            $message="Project Owner info save successfully";
            return $message;
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    
    
    public function select_all_project_owner_info() {
        $sql="SELECT * FROM tbl_project_owner WHERE publication_status='1'";
        if(mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    
    public function select_all_project_owner_info_to_manage() {
        $sql="SELECT * FROM tbl_project_owner";
        if(mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    
    public function unpublished_project_owner_info_by_id($project_owner_id) {
        $sql="UPDATE tbl_project_owner SET publication_status = 0 WHERE project_owner_id = '$project_owner_id' ";
        if(mysqli_query($this->link, $sql)) {
            $message='Project Owner info unpublished successfully';
            return $message;
           // header('Loication: manage_category.php');
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    public function published_project_owner_info_by_id($project_owner_id) {
        $sql="UPDATE tbl_project_owner SET publication_status = 1 WHERE project_owner_id = '$project_owner_id' ";
        if(mysqli_query($this->link, $sql)) {
             $message='Project owner info published successfully';
            return $message;
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    public function edit_project_owner_info_by_id($project_owner_id) {
        $sql="SELECT * FROM tbl_project_owner WHERE project_owner_id = '$project_owner_id' ";
        if(mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    public function update_project_owner_info_by_id($data) {
        $project_owner_id=$_GET['id'];
        extract($data);
        $sql="UPDATE tbl_project_owner SET project_owner_name = '$project_owner_name',edison_id='$edison_id',email_address='$email_address', project_owner_description = '$project_owner_description', publication_status = '$publication_status' WHERE project_owner_id = '$project_owner_id' ";
        if(mysqli_query($this->link, $sql)) {
            $_SESSION['message']='project owner info update successfully';
            header('Location: manage_project_owner.php');
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    public function delete_project_owner_info_by_id($project_owner_id) {
         $sql="DELETE FROM tbl_project_owner WHERE project_owner_id = '$project_owner_id' ";
        if(mysqli_query($this->link, $sql)) {
            $message = 'Delete successfully';
            return $message;
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
}
