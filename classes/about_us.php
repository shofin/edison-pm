<?php
require_once 'db_connect.php';

class About_us extends Db_connect {

    public $link;

    public function __construct() {
        $this->link = $this->database_connection();
    }

    public function save_about_us_info($data) {
        extract($data);
        $sql = "INSERT INTO tbl_about_us (Content_short_description, multi_capability,about_us_video, publication_status) VALUES ('$Content_short_description', '$multi_capability','$about_us_video', '$publication_status' )";
        if (mysqli_query($this->link, $sql)) {
            $message = "About us info save successfully";
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function select_all_published_about_us_info() {
        $sql = "SELECT * FROM tbl_about_us WHERE publication_status = 1";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function select_all_about_us_info() {
        $sql = "SELECT * FROM tbl_about_us";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function unpublished_about_us_info_by_id($about_us_id) {
        $sql = "UPDATE tbl_about_us SET publication_status = 0 WHERE about_us_id = '$about_us_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'About us info unpublished successfully';
            return $message;
            // header('Loication: manage_category.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function published_about_us_info_by_id($about_us_id) {
        $sql = "UPDATE tbl_about_us SET publication_status = 1 WHERE about_us_id = '$about_us_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'About us info published successfully';
            return $message;
            //header('Loication: manage_category.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    
    public function delete_about_us_info_by_id($about_us_id) {
        $sql = "DELETE FROM tbl_about_us WHERE about_us_id = '$about_us_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'About us info DELETE successfully';
            return $message;
            //header('Loication: manage_category.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    

}
