<?php
require_once 'db_connect.php';

class Flagship extends Db_Connect{
    public $link;

    public function __construct() {
        $this->link = $this->database_connection();
    }
    
    public function save_flagship_model_info($data){
        extract($data);
        $directory = '../assets/images/flagship/';
        $target_file = $directory . $_FILES['flagship_image']['name'];
        $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
        $file_size = $_FILES['flagship_image']['size'];
        $check = getimagesize($_FILES['flagship_image']['tmp_name']);
        if ($check) {
            if (file_exists($target_file)) {
                die("This file already uploaded. plese try one another");
            } else {
                if ($file_type != 'jpg' && $file_type != 'png') {
                    die('The file type is not valid. Please use jpg or png');
                } else {

                    if ($file_size > 5000000000000) {
                        die("File size is too large. use with in 500mb");
                    } else {
                        move_uploaded_file($_FILES['flagship_image']['tmp_name'], $target_file);
                        $sql = "INSERT INTO tbl_flagship_models (flagship_title, flagship_model_name,short_description,key_feature,flagship_image,flagship_video, publication_status) VALUES ('$flagship_title', '$flagship_model_name','$short_description','$key_feature','$target_file','$flagship_video', '$publication_status' )";
                        if (mysqli_query($this->link, $sql)) {
                            $message = "Flagship info save successfully";
                            return $message;
                        } else {
                            die('Query problem' . mysqli_error($this->link));
                        }
                    }
                }
            }
        } else {
            die('The file you upload is not image. Plese use valid image file');
        }
    }
    
    public function select_all_published_flagship() {
        $sql = "SELECT * FROM tbl_flagship_models WHERE publication_status = 1";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    
     public function select_all_flagship_info() {
        $sql = "SELECT * FROM tbl_flagship_models";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    
    
    public function select_flagship_info_by_id($flagship_id) {
        $sql="SELECT * FROM tbl_flagship_models WHERE flagship_id = '$flagship_id' ";
        if(mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    
    
    public function unpublished_flagship_info_by_id($flagship_id) {
        $sql = "UPDATE tbl_flagship_models SET publication_status = 0 WHERE flagship_id = '$flagship_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'flagship info unpublished successfully';
            return $message;
            // header('Loication: manage_category.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function published_flagship_info_by_id($flagship_id) {
        $sql = "UPDATE tbl_flagship_models SET publication_status = 1 WHERE flagship_id = '$flagship_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'flagship info published successfully';
            return $message;
            //header('Loication: manage_category.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    
    public function update_flagship_info_by_id($_data){
        extract($_data);
        $flagship_id = $_GET['id'];
        
        $new_image = $_FILES['flagship_image']['name'];
        if ($new_image) {
            $directory = '../assets/images/flagship/';
            $target_file = $directory . $_FILES['flagship_image']['name'];
            $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
            $file_size = $_FILES['flagship_image']['size'];
            $check = getimagesize($_FILES['flagship_image']['tmp_name']);
            if ($check) {
                if (file_exists($target_file)) {
                    die("This file already uploaded. plese try one another");
                } else {
                    if ($file_type != 'jpg' && $file_type != 'png') {
                        die('The file type is not valid. Please use jpg or png');
                    } else {

                        if ($file_size > 5000000000000) {
                            die("File size is too large. use with in 500mb");
                        } else {
                            move_uploaded_file($_FILES['flagship_image']['tmp_name'], $target_file);
                            $sql = "UPDATE tbl_flagship_models SET flagship_title= '$flagship_title', flagship_model_name='$flagship_model_name',short_description='$short_description',key_feature='$key_feature',flagship_image='$target_file',flagship_video='$flagship_video', publication_status= '$publication_status' WHERE flagship_id = '$flagship_id'";
                            if (mysqli_query($this->link, $sql)) {
                                $_SESSION['message'] = "Flagship Info Update Successfully";
                                header('Location: manage_flagship_models.php');
                            } else {
                                die('Query problem' . mysqli_error($this->link));
                            }
                        }
                    }
                }
            } else {
                die('The file you upload is not image. Plese use valid image file');
            }
        } else {
            
            $sql = "UPDATE tbl_flagship_models SET flagship_title= '$flagship_title', flagship_model_name='$flagship_model_name',short_description='$short_description',key_feature='$key_feature',flagship_video='$flagship_video', publication_status= '$publication_status' WHERE flagship_id = '$flagship_id'";
            if (mysqli_query($this->link, $sql)) {
                $_SESSION['message'] = "Flagship Info Update Successfully";
                header('Location: manage_flagship_models.php');
            } else {
                die('Query problem' . mysqli_error($this->link));
            }
        }
    }
    
    public function delete_flagship_info_by_id($flagship_id) {
         $sql="DELETE FROM tbl_flagship_models WHERE flagship_id = '$flagship_id'";
        if(mysqli_query($this->link, $sql)) {
            $_SESSION['message']='Flagship info delete successfully';
            header('Location: manage_flagship_models.php');
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    
} 