<?php

class Slider extends Db_Connect {

    public $link;

    public function __construct() {
        $this->link = $this->database_connection();
    }

    public function save_slider_info($data) {
        extract($data);
        $directory = '../assets/images/main_slider/';
        $target_file = $directory . $_FILES['slider_image']['name'];
        $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
        $file_size = $_FILES['slider_image']['size'];
        $check = getimagesize($_FILES['slider_image']['tmp_name']);
        if ($check) {
            if (file_exists($target_file)) {
                die("This file already uploaded. plese try one another");
            } else {
                if ($file_type != 'jpg' && $file_type != 'png') {
                    die('The file type is not valid. Please use jpg or png');
                } else {

                    if ($file_size > 5000000000000) {
                        die("File size is too large. use with in 500mb");
                    } else {
                        move_uploaded_file($_FILES['slider_image']['tmp_name'], $target_file);
                        $sql = "INSERT INTO tbl_slider (slider_name, slider_title,slider_description,slider_image, publication_status) VALUES ('$slider_name', '$slider_title','$slider_description','$target_file', '$publication_status' )";
                        if (mysqli_query($this->link, $sql)) {
                            $message = "slider info save successfully";
                            return $message;
                        } else {
                            die('Query problem' . mysqli_error($this->link));
                        }
                    }
                }
            }
        } else {
            die('The file you upload is not image. Plese use valid image file');
        }
    }

    public function select_all_published_slider() {
        $sql = "SELECT * FROM tbl_slider WHERE publication_status = 1";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function select_all_slider_info() {
        $sql = "SELECT * FROM tbl_slider";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function unpublished_slider_info_by_id($slider_id) {
        $sql = "UPDATE tbl_slider SET publication_status = 0 WHERE slider_id = '$slider_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'Slider info unpublished successfully';
            return $message;
            // header('Loication: manage_category.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function published_slider_info_by_id($slider_id) {
        $sql = "UPDATE tbl_slider SET publication_status = 1 WHERE slider_id = '$slider_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'Slider info published successfully';
            return $message;
            //header('Loication: manage_category.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function select_slider_info_by_id($slider_id) {
        $sql = "SELECT * FROM tbl_slider WHERE slider_id = '$slider_id' ";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function update_slider_info_by_id($data) {
        extract($data);
        $new_image = $_FILES['slider_image']['name'];
        if ($new_image) {
            $directory = '../assets/images/main_slider/';
            $target_file = $directory . $_FILES['slider_image']['name'];
            $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
            $file_size = $_FILES['slider_image']['size'];
            $check = getimagesize($_FILES['slider_image']['tmp_name']);
            if ($check) {
                if (file_exists($target_file)) {
                    die("This file already uploaded. plese try one another");
                } else {
                    if ($file_type != 'jpg' && $file_type != 'png') {
                        die('The file type is not valid. Please use jpg or png');
                    } else {

                        if ($file_size > 10000000000000) {
                            die("File size is too large. use with in 500mb");
                        } else {
                            move_uploaded_file($_FILES['slider_image']['tmp_name'], $target_file);
                            $sql = "UPDATE tbl_slider SET slider_name= '$slider_name', slider_title='$slider_title',slider_description='$slider_description',slider_image='$target_file', publication_status= '$publication_status' WHERE slider_id= '$slider_id'";
                            if (mysqli_query($this->link, $sql)) {
                                $_SESSION['message'] = "Slider Info Update Successfully";
                                header('Location: manage_slider.php');
                            } else {
                                die('Query problem' . mysqli_error($this->link));
                            }
                        }
                    }
                }
            } else {
                die('The file you upload is not image. Plese use valid image file');
            }
        } else {
            $sql = "UPDATE tbl_slider SET slider_name= '$slider_name', slider_title='$slider_title',slider_description='$slider_description', publication_status= '$publication_status' WHERE slider_id= '$slider_id'";
            if (mysqli_query($this->link, $sql)) {
                $_SESSION['message'] = "Slider Info Update Successfully";
                header('Location: manage_slider.php');
            } else {
                die('Query problem' . mysqli_error($this->link));
            }
        }
    }
    
    public function delete_slider_info_by_id($slider_id) {
         $sql="DELETE FROM tbl_slider WHERE slider_id = '$slider_id' ";
        if(mysqli_query($this->link, $sql)) {
            $_SESSION['message']='Category info delete successfully';
            header('Location: manage_slider.php');
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }

}
