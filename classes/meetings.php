<?php
require_once 'db_connect.php';

class Meetings extends Db_connect {
    public $link;

    public function __construct() {
        $this->link = $this->database_connection();
    }
    
    public function save_meeting_minutes_info($data) {
        extract($data);
        $directory = '../assets/images/meeting/';
        $target_file = $directory . $_FILES['meeting_minutes_image']['name'];
        $report_file = $directory . $_FILES['meeting_minutes_file']['name'];
        $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
        $file_size = $_FILES['meeting_minutes_image']['size'];
        $check = getimagesize($_FILES['meeting_minutes_image']['tmp_name']);
        if ($check) {
            if (file_exists($target_file)) {
                die("This file already uploaded. plese try one another");
            } else {
                if ($file_type != 'jpg' && $file_type != 'png') {
                    die('The file type is not valid. Please use jpg or png');
                } else {

                    if ($file_size > 500000000) {
                        die("File size is too large. use with in 5mb");
                    } else {
                        move_uploaded_file($_FILES['meeting_minutes_image']['tmp_name'], $target_file);
                        move_uploaded_file($_FILES['meeting_minutes_file']['tmp_name'], $report_file);
                        $sql = "INSERT INTO tbl_meetings (meeting_minutes_image, duration,chair, subject,short_description,area_1,decision_1,responsible_1,target_date_1,area_2,decision_2,responsible_2,target_date_2,area_3,decision_3,responsible_3,target_date_3,remarks,meeting_minutes_file,publication_status) VALUES "
                                . "('$target_file','$duration','$chair', '$subject','$short_description','$area_1','$decision_1','$responsible_1','$target_date_1','$area_2','$decision_2','$responsible_3','$target_date_2', '$area_3','$decision_3', '$responsible_3','$target_date_3', '$remarks', '$report_file', '$publication_status' )";
                    if (mysqli_query($this->link, $sql)) {
                            $message = "Isert Successfully";
                            return $message;
                        } else {
                            die('Query problem' . mysqli_error($this->link));
                        }
                    }
                }
            }
        } else {
            die('The file you upload is not valid image. Plese use valid image file');
        }
    }
    public function select_all_meetings() {
        $sql = "SELECT meetings_id,date,subject,short_description,publication_status FROM tbl_meetings";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    
    public function view_meetings_by_id($meetings_id) {
        $sql = "SELECT * FROM tbl_meetings WHERE meetings_id= '$meetings_id' ";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function unpublished_meetings_by_id($meetings_id) {
        $sql = "UPDATE tbl_meetings SET publication_status = 0 WHERE meetings_id = '$meetings_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'Unpublished successfully';
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function published_meetings_by_id($meetings_id) {
        $sql = "UPDATE tbl_meetings SET publication_status = 1 WHERE meetings_id = '$meetings_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'Published successfully';
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function delete_meetings_by_id($meetings_id) {
        $sql = "DELETE FROM tbl_meetings WHERE meetings_id = '$meetings_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'Delete successfully';
            return $message;
            
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    
    public function select_all_published_meetings_info() {
        $sql = "SELECT meetings_id,meeting_minutes_image,date,subject,short_description,meeting_minutes_file FROM tbl_meetings WHERE publication_status = 1";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    
    public function details_meeting_minutes_info_by_id($meetings_id) {
        $sql = "SELECT * FROM tbl_meetings WHERE meetings_id = $meetings_id";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function save_news_feed($data) {
        extract($data);
        $sql = "INSERT INTO tbl_news_feed (news_date_time, news_title, news_contents,publication_status)VALUES('$news_date_time','$news_title','$news_contents','$publication_status')";
        if (mysqli_query($this->link, $sql)) {
            $message= 'News Added Successfully';
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
   
    
    public function select_all_news_feed() {
        $sql = "SELECT * FROM tbl_news_feed";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function unpublished_news_feed_by_id($news_feed_id) {
        $sql = "UPDATE tbl_news_feed SET publication_status = 0 WHERE news_feed_id = '$news_feed_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'News unpublished successfully';
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function published_news_feed_by_id($news_feed_id) {
        $sql = "UPDATE tbl_news_feed SET publication_status = 1 WHERE news_feed_id = '$news_feed_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'News published successfully';
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function delete_news_feed_by_id($news_feed_id) {
        $sql = "DELETE FROM tbl_news_feed WHERE news_feed_id = '$news_feed_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'Delete news successfully';
            return $message;
            
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function select_all_published_news_feed_info() {
        $sql = "SELECT * FROM tbl_news_feed WHERE publication_status = 1 ORDER BY  news_feed_id DESC LIMIT 2";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    
    public function save_knowledge_share_info($data) {
        extract($data);
        $directory = '../assets/files/knowledge_share/';
        $target_file = $directory . $_FILES['knowledge_share_file']['name'];
        $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
        $file_size = $_FILES['knowledge_share_file']['size'];
            if (file_exists($target_file)) {
                die("This file already uploaded. plese try one another");
            } else {
                if ($file_type != 'docx' && $file_type != 'pptx' && $file_type != 'pdf' ) {
                    die('The file type is not valid. Please use docx or slsx or pptx');
                } else {
                    if ($file_size > 5000000000) {
                        die("File size is too large. use with in 5mb");
                    } else {
                        move_uploaded_file($_FILES['knowledge_share_file']['tmp_name'], $target_file);
                        $sql = "INSERT INTO tbl_knowledge_share (date_time,publisher_name,knowledge_share_title,knowledge_share_contents,knowledge_share_file,publication_status) VALUES "
                                . "('$date_time','$publisher_name','$knowledge_share_title','$knowledge_share_contents','$target_file','$publication_status' )";
                    if (mysqli_query($this->link, $sql)) {
                            $message = "Isert Successfully";
                            return $message;
                        } else {
                            die('Query problem' . mysqli_error($this->link));
                        }
                    }
                }
            } 
    }
    
    public function select_all_knowledge_share_info() {
        $sql = "SELECT * FROM tbl_knowledge_share";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function unpublished_knowledge_share_by_id($knowledge_share_id) {
        $sql = "UPDATE tbl_knowledge_share SET publication_status = 0 WHERE knowledge_share_id = '$knowledge_share_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'Knowledge share unpublished successfully';
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function published_knowledge_share_by_id($knowledge_share_id) {
        $sql = "UPDATE tbl_knowledge_share SET publication_status = 1 WHERE knowledge_share_id = '$knowledge_share_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'Knowledge share published successfully';
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function delete_knowledge_share_by_id($knowledge_share_id) {
        $sql = "DELETE FROM tbl_knowledge_share WHERE knowledge_share_id = '$knowledge_share_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'Delete successfully';
            return $message;
            
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function select_all_published_knowledge_share_info() {
        $sql = "SELECT * FROM tbl_knowledge_share WHERE publication_status = 1 ORDER BY  knowledge_share_id DESC LIMIT 1";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
}
