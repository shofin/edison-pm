<?php

class Odm extends Db_connect {

    public $link;

    public function __construct() {
        $this->link = $this->database_connection();
    }

    public function save_odm_info($data) {
        extract($data);
        $sql = "INSERT INTO tbl_odm (odm_name,contact_1,contact_2,odm_description, publication_status) VALUES ('$odm_name','$contact_1','$contact_2','$odm_description', '$publication_status' )";
        if (mysqli_query($this->link, $sql)) {
            $message = "ODM info save successfully";
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function select_all_odm() {

        $sql = "SELECT * FROM tbl_odm ";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function unpublished_odm_by_id($odm_id) {
        $sql = "UPDATE tbl_odm SET publication_status = 0 WHERE odm_id = '$odm_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'ODM info unpublished successfully';
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function published_odm_by_id($odm_id) {
        $sql = "UPDATE tbl_odm SET publication_status = 1 WHERE odm_id = '$odm_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'ODM info published successfully';
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function delete_odm_by_id($odm_id) {
         $sql="DELETE FROM tbl_odm WHERE odm_id = '$odm_id' ";
        if(mysqli_query($this->link, $sql)) {
            $message = 'Delete ODM successfully';
            return $message;
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }

    public function select_all_odm_info() {
        $sql = "SELECT * FROM tbl_odm WHERE publication_status='1'";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function select_odm_by_id($odm_id) {
        $sql = "SELECT * FROM tbl_odm WHERE odm_id ='$odm_id'";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function update_odm_info_by_id($data) {
        $odm_id=$_POST['odm_id'];
        extract($data);
        $sql="UPDATE tbl_odm SET odm_name = '$odm_name', contact_1 = '$contact_1',contact_2 = '$contact_2',odm_description='$odm_description',publication_status = '$publication_status' WHERE odm_id = '$odm_id' ";
        if(mysqli_query($this->link, $sql)) {
            $_SESSION['message']='ODM info update successfully';
            header('Location: manage_odm.php');
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }

}
