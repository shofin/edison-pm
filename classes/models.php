<?php

class Models extends Db_connect{
    public $link;

    public function __construct() {
        $this->link = $this->database_connection();
    }
    public function save_models_info($data) {
        extract($data);
        $sql="INSERT INTO tbl_model_info (model_name, phone_type, project_owner_id,odm_id,spec_and_npd,po_date,launching_year) VALUES ('$model_name', '$phone_type', '$project_owner_id', '$odm_id','$spec_and_npd','$po_date','$launching_year' )";
        if(mysqli_query($this->link, $sql)) {
            $message="Models info save successfully";
            return $message;
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    public function select_all_models_info() {
        $sql="SELECT mo.*,po.project_owner_name,o.odm_name FROM tbl_model_info as mo INNER JOIN tbl_project_owner po ON po.project_owner_id=mo.project_owner_id INNER JOIN tbl_odm o ON o.odm_id=mo.odm_id ";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function edit_model_info_by_id($model_id) {
        $sql = "SELECT * FROM tbl_model_info WHERE model_id=$model_id";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function update_model_info_by_id($data) {
        extract($data);
        $sql = "UPDATE tbl_model_info SET model_name = '$model_name', phone_type = '$phone_type', project_owner_id = '$project_owner_id', odm_id='$odm_id',spec_and_npd='$spec_and_npd',po_date='$po_date',launching_year='$launching_year' WHERE model_id = '$model_id'";
        if (mysqli_query($this->link, $sql)) {
            $_SESSION['message'] = 'Model info update successfully';
            header('Location: manage_model.php');
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
}
