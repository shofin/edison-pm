<?php

class pds extends Db_connect{
    public $link;

    public function __construct() {
        $this->link = $this->database_connection();
    }
    
    public function save_pds_info($data) {
        extract($data);

            $directory = '../assets/images/user/';
            $target_file1 = $directory . $_FILES['pds_files']['name'];
            $target_file2 = $directory . $_FILES['user_manual']['name'];
            $file_type1 = pathinfo($target_file1, PATHINFO_EXTENSION);
            $file_type2 = pathinfo($target_file2, PATHINFO_EXTENSION);
            $file_size1 = $_FILES['pds_files']['size'];
            $file_size2 = $_FILES['user_manual']['size'];
//            $check = getimagesize($_FILES['pds_files']['tmp_name']);
            
//            if ($check) {
                if (file_exists($target_file1 && $target_file2)) {
                    die("This file already uploaded. plese try one another");
                } else {
                    if ($file_type1 && $file_type2!= 'xlsx' && $file_type1&& $file_type2 != 'txt') {
                        die('The file type is not valid. Please use xlsx or txt');
                    } else {

                        if ($file_size1 && $file_size2 > 500000000000) {
                            die("File size is too large. use with in 500mb");
                        } else {
                            move_uploaded_file($_FILES['pds_files']['tmp_name'], $target_file1);
                            move_uploaded_file($_FILES['user_manual']['tmp_name'], $target_file2);
                            $sql = "INSERT INTO tbl_pds (pds_files,user_manual) VALUES ('$target_file1','$target_file2' )";
                            if (mysqli_query($this->link, $sql)) {
                                $message = "File Upload Success";
                                return $message;
                            } else {
                                die('Query problem' . mysqli_error($this->link));
                            }
                        }
                    }
                }
//            } else {
//                die('The file you upload is not image. Plese use valid image file');
//            }
    }
    
    public function select_all_files() {
        $sql = "SELECT * FROM tbl_pds";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
}
