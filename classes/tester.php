<?php

class Tester extends Db_connect{
    public $link;
    public function __construct() {
        $this->link=$this->database_connection();
    }
    public function save_tester_info($data) {
        extract($data);
        $sql="INSERT INTO tbl_tester (tester_name,tester_email_address, tester_description, publication_status) VALUES ('$tester_name','$tester_email_address', '$tester_description', '$publication_status' )";
        if(mysqli_query($this->link, $sql)) {
            $message="Tester info save successfully";
            return $message;
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    public function select_all_tester_info() {
        $sql="SELECT * FROM tbl_tester ORDER BY tester_id ASC";
        if(mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    public function unpublished_tester_info_by_id($tester_id) {
        $sql="UPDATE tbl_tester SET publication_status = 0 WHERE tester_id = '$tester_id' ";
        if(mysqli_query($this->link, $sql)) {
            $message='Tester info unpublished successfully';
            return $message;
           // header('Loication: manage_category.php');
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    public function published_tester_info_by_id($tester_id) {
        $sql="UPDATE tbl_tester SET publication_status = 1 WHERE tester_id = '$tester_id' ";
        if(mysqli_query($this->link, $sql)) {
            $message='Tester info published successfully';
            return $message;
           // header('Loication: manage_category.php');
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    public function delete_tester_info_by_id($tester_id) {
        $sql = "DELETE FROM tbl_tester WHERE tester_id = '$tester_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = 'Tester Info Delete Successfully';
            return $message;
            
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    
    public function select_all_published_tester_info() {
        $sql="SELECT * FROM tbl_tester WHERE publication_status='1'";
        if(mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    
    
}
