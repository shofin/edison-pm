<?php

require_once 'db_connect.php';

class Login_signup extends Db_connect {

    public $link;

    public function __construct() {
        $this->link = $this->database_connection();
    }

    public function user_login_check($data) {
        extract($data);
        $password = md5($password);
        $query = "SELECT * FROM tbl_user WHERE email_address= '$email_address' AND password= '$password' AND approve_status='1' ";

        $query_result = mysqli_query($this->link, $query);
        $user_info = mysqli_fetch_assoc($query_result);
        if ($user_info) {
            session_start();
            $_SESSION['user_name'] = $user_info['user_name'];
            $_SESSION['user_id'] = $user_info['user_id'];
            $_SESSION['pm_member'] = $user_info['pm_member'];

            header('Location: user_master.php');
        } else {
            $message = "Please use valid email address & password";
            return $message;
        }
    }

    public function user_logout() {
        unset($_SESSION['user_name']);
        unset($_SESSION['user_id']);
        unset($_SESSION['pm_member']);

        header('Location: index.php');
    }
    
    public function pm_user_logout() {
        unset($_SESSION['user_name']);
        unset($_SESSION['user_id']);
        unset($_SESSION['pm_member']);

        header('Location: ../index.php');
    }

    public function new_user_info($data) {
        extract($data);

        $password = md5($password);
        if ($data['password'] == $data['confirm_password']) {

            $directory = 'assets/images/user/';
            $target_file = $directory . $_FILES['user_image']['name'];
            $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
            $file_size = $_FILES['user_image']['size'];
            $check = getimagesize($_FILES['user_image']['tmp_name']);
            if ($check) {
                if (file_exists($target_file)) {
                    die("This file already uploaded. plese try one another");
                } else {
                    if ($file_type != 'jpg' && $file_type != 'png') {
                        die('The file type is not valid. Please use jpg or png');
                    } else {

                        if ($file_size > 500000000000000000) {
                            die("File size is too large. use with in 500mb");
                        } else {
                            move_uploaded_file($_FILES['user_image']['tmp_name'], $target_file);
                            $sql = "INSERT INTO tbl_user (user_name, employe_id, phone_number,email_address,password,pin_number,career_short_description,user_image) VALUES ('$user_name', '$employe_id', '$phone_number','$email_address','$password','$pin_number','$career_short_description', '$target_file' )";
                            if (mysqli_query($this->link, $sql)) {
                                $message = "Please wait untill approved.A confirmation maill will be send after verification";
                                return $message;
                            } else {
                                die('Query problem' . mysqli_error($this->link));
                            }
                        }
                    }
                }
            } else {
                die('The file you upload is not image. Plese use valid image file');
            }
        } else {
            session_start();
            $_SESSION['message'] = 'password not match';
        }
    }

    public function admin_login_check($data) {
        $password = md5($data['password']);
        $query = "SELECT * FROM tbl_admin WHERE email_address = '$data[email_address]' AND password= '$password' ";

        $query_result = mysqli_query($this->link, $query);
        $admin_info = mysqli_fetch_assoc($query_result);
        if ($admin_info) {
            session_start();
            $_SESSION['admin_name'] = $admin_info['admin_name'];
            $_SESSION['admin_id'] = $admin_info['admin_id'];

            header('Location: admin_master.php');
        } else {
            $message = "Please use valid email address & password";
            return $message;
        }
    }

    public
            function admin_logout() {
        unset($_SESSION['admin_name']);
        unset($_SESSION['admin_id']);

        header('Location: index.php');
    }

}
