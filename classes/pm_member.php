<?php
require_once 'db_connect.php';

class Pm_member extends Db_connect{
   public $link;

    public function __construct() {
        $this->link = $this->database_connection();
    }
    
    public function select_pm_user_info() {
        $sql = "SELECT * FROM tbl_user WHERE approve_status = 1 AND pm_member = 1";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    
}
