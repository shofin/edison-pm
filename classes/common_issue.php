<?php

class Common_issue extends Db_connect{
    public $link;

    public function __construct() {
        $this->link = $this->database_connection();
    }

    public function save_common_issue_info($data) {
        $added_by=$_SESSION['admin_name'];
        extract($data);
        $sql = "INSERT INTO tbl_common_issue (project_id, project_owner_id,odm_id, chipset,subject,issue_details,remarks,added_by) VALUES ('$project_id', '$project_owner_id','$odm_id', '$chipset','$subject','$issue_details','$remarks','$added_by' )";
        if (mysqli_query($this->link, $sql)) {
            $message = "Common Issue save successfully";
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    
    public function select_all_common_issue_info() {
        $sql = "SELECT c.*,p.project_name,po.project_owner_name,o.odm_name FROM tbl_common_issue as c INNER JOIN tbl_project p ON p.project_id = c.project_id INNER JOIN tbl_project_owner po ON po.project_owner_id = c.project_owner_id INNER JOIN tbl_odm o ON o.odm_id = c.odm_id ";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function select_common_issue_by_id($common_issue_id) {
        $sql = "SELECT c.*,p.project_name,po.project_owner_name,o.odm_name FROM tbl_common_issue as c INNER JOIN tbl_project p ON p.project_id = c.project_id INNER JOIN tbl_project_owner po ON po.project_owner_id = c.project_owner_id INNER JOIN tbl_odm o ON o.odm_id = c.odm_id WHERE c.common_issue_id='$common_issue_id'";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function update_common_issue_by_id($data) {
        extract($data);
        $sql="UPDATE tbl_common_issue SET project_id = '$project_id',project_owner_id = '$project_owner_id', odm_id = '$odm_id', chipset = '$chipset',subject='$subject',issue_details='$issue_details',remarks='$remarks' WHERE common_issue_id = '$common_issue_id' ";
        if(mysqli_query($this->link, $sql)) {
            $_SESSION['message']='Common issue update successfully';
            header('Location: manage_common_issue.php');
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    public function delete_common_issue_by_id($common_issue_id) {
         
        $sql="DELETE FROM tbl_common_issue WHERE common_issue_id = '$common_issue_id' ";
        if(mysqli_query($this->link, $sql)) {
            $_SESSION['delete']=' Info delete successfully';
            header('Location: manage_common_issue.php');
        } else {
            die('Query problem'.mysqli_error($this->link) );
        }
    }
    
    public function search_common_issue($search_q) {
        $sql = "SELECT c.*,p.project_name,po.project_owner_name,o.odm_name FROM tbl_common_issue as c INNER JOIN tbl_project p ON p.project_id = c.project_id INNER JOIN tbl_project_owner po ON po.project_owner_id = c.project_owner_id INNER JOIN tbl_odm o ON o.odm_id = c.odm_id WHERE c.chipset LIKE '%$search_q%' ";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
}
