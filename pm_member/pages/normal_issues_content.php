<?php
$query_result = $obj_normal_issue->select_all_normal_issue();

?>
<div class="panel-body">
    <form class="form-horizontal" action="search_normal_issues.php" method="post">
        <div class="form-group">
            <div class="col-lg-3">
                <input type="text" name="search" placeholder="Search By Chipset" required class="form-control"/>   
            </div>
            <div class="col-lg-2">
                <input type="submit" name="submit" value="Search" class="form-control"/>
            </div>
        </div>
    </form>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading text-center lead">
                All Normal Issues Goes Here
            </div>
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SL NO</th>
                            <th>Project Name</th>
                            <th>ODM Name</th>
                            <th>Chipset</th>
                            <th>Title </th>
                            <th>Issue Type</th>
                            <th>Platform</th>
                            <th>Remarks</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($all_normal_issue = mysqli_fetch_assoc($query_result)) {
                            extract($all_normal_issue);
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $project_name; ?></td>
                                <td><?php echo $odm_name; ?></td>
                                <td><?php echo $chipset; ?></td>
                                <td><?php echo $title_of_issue; ?></td>
                                <td class="center"><?php
                                    if ($issue_type == 1) {
                                        echo 'Chipset';
                                    } else if ($issue_type == 2) {
                                        echo 'OS';
                                    } else {
                                        echo 'N/A';
                                    }
                                    ?></td>
                                <td><?php echo $platform; ?></td>
                                <td><?php echo $remarks; ?></td>
                                <td class="center">
                                    <a href="view_normal_issue.php?id=<?php echo $normal_issue_id; ?>" class="btn btn-success" title="Edit">
                                        <span class="glyphicon glyphicon-zoom-in"></span>
                                    </a>
                                    
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>