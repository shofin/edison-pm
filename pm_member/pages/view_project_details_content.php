<?php
$project_summary_id = $_GET['id'];
$query_result = $obj_project_summary->details_project_summary_info_by_id($project_summary_id);
//$project_details=mysqli_fetch_assoc($query_result);
//extract($project_details);
//while ($project_summary = mysqli_fetch_assoc($query_result)) {
//    echo '<pre>';
//    print_r($project_summary);
//}
//exit();
?>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading text-center lead">
                Project Details Information Goes Here<br>
                <a href="all_project_summary.php" class="btn btn-primary" title="Back" >
                    <span class="glyphicon glyphicon-backward"></span>
                </a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <?php
                    while ($project_summary = mysqli_fetch_assoc($query_result)) {
                        extract($project_summary);
                        ?>    
                        <tr>
                            <td>Project Name</td>
                            <td><?php echo $project_name; ?></td>
                        </tr>
                        <tr>
                            <td>Project Type</td>
                            <td><?php
                                if ($project_type == 1) {
                                    echo 'Smart Phone/TAB';
                                } else {
                                    echo 'Feature Phone';
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <td>ODM Name</td>
                            <td><?php echo $odm_name; ?></td>
                        </tr>
                        <tr>
                            <td>Developer Name</td>
                            <td><?php echo $developer_name; ?></td>
                        </tr>

                        <tr>
                            <td>Project Owner Name</td>
                            <td><?php echo $project_owner_name; ?></td>
                        </tr>
                        <tr>
                            <td>Tester Name</td>
                            <td><?php echo $tester_1_name . ', ' . $tester_2_name . ', ' . $tester_3_name . ', ' . $tester_4_name; ?></td>
                        </tr>
                        <tr>
                            <td>Hardware Version</td>
                            <td><?php echo 'HW' . $hardware_version; ?></td>
                        </tr>
                        <tr>
                            <td>Current soft version</td>
                            <td><?php echo 'V' . $current_soft_version; ?></td>
                        </tr>

                        <tr>
                            <td>Total Issue</td>
                            <td><?php echo $total_issue; ?></td>
                        </tr>
                        <tr>
                            <td>Resolve Issue</td>
                            <td><?php echo $resolve_issue; ?></td>
                        </tr>
                        <tr>
                            <td>Open Issue</td>
                            <td><?php echo $open_issue; ?></td>
                        </tr>
                        <tr>
                            <td>Normal Issue</td>
                            <td><?php echo $normal_issue; ?></td>
                        </tr>
                        <tr>
                            <td>After Sales Issue</td>
                            <td><?php echo $after_sales_issue; ?></td>
                        </tr>
                        <tr>
                            <td>After Sales Resolve Issue</td>
                            <td><?php echo $after_sales_resolve_issue; ?></td>
                        </tr>
                        <tr>
                            <td>Confirm Soft. Version</td>
                            <td><?php
                                if (!empty($confirm_soft_version)) {
                                    echo 'V' . $confirm_soft_version;
                                }else{
                                    echo 'Not Confirmed Yet';
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <td>Confirmation Date</td>
                            <td><?php echo $confirmation_date; ?></td>
                        </tr>
                        <tr>
                            <td>Shipment Date</td>
                            <td><?php echo $shipment_date; ?></td>
                        </tr>
                        <tr>
                            <td>Any Difficulties</td>
                            <td><?php echo $any_difficulties; ?></td>
                        </tr>
                        <tr>
                            <td>Models Image</td>
                            <td><img src="<?php echo $models_image; ?>" height="80" width="100" alt=" models image"/> </td>
                        </tr>
                        <tr>
                            <td>Final PDS</td>
                            <td><a href="<?php echo $final_pds; ?> "target=”_blank” >Final PDS File</a></td>
                        </tr>
<?php } ?>
                </table>
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
            <div class="panel-heading text-center lead">
                <a href="all_project_summary.php" class="btn btn-primary" title="Back" >
                    <span class="glyphicon glyphicon-backward"></span>
                </a>
                <a href="project_summary_print.php?id=<?php echo $project_summary_id; ?>" class="btn btn-success" title="Back" >
                    <span class="glyphicon glyphicon-print"></span>
                </a>
            </div>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
