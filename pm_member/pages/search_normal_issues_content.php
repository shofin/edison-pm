<?php
if(empty($_POST['search'])){
    header('Location: normal_issues.php');
}
$search_q = $_POST['search'];
$search_q = preg_replace('#[^0-9a-z]#i', '', $search_q);
$query_result = $obj_normal_issue->search_normal_issue($search_q);
?>
<div class="panel-body">
    <form class="form-horizontal" action="search_normal_issues.php" method="post">
        <div class="form-group">
            <div class="col-lg-3">
                <input type="text" name="search" placeholder="Search By Chipset" required class="form-control"/>   
            </div>
            <div class="col-lg-2">
                <input type="submit" name="submit" value="Search" class="form-control"/>
            </div>
        </div>
    </form>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading text-center lead">
                All Normal Issues By Chipset
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="NormalIssue-Chipset">
                    <thead>
                        <tr>
                            <th>SL NO</th>
                            <th>Project Name</th>
                            
                            <th>ODM Name</th>
                            <th>Chipset</th>
                            <th>Tile </th>
                            <th>Issue Type</th>
                            <th>remarks</th>
                            <th>Added By</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $count = mysqli_num_rows($query_result);
                        if ($count == 0) {
                            echo '<div>
        <h3 class="center text text-danger"> No Result Found ): </h3>
    </div>';
                        } else {
                            $i = 1;
                            while ($all_normal_issue_by_chipset = mysqli_fetch_assoc($query_result)) {
                                extract($all_normal_issue_by_chipset);
                                ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $project_name; ?></td>
                                    
                                    <td><?php echo $odm_name; ?></td>
                                    <td><?php echo $chipset; ?></td>
                                    <td><?php echo $title_of_issue; ?></td>
                                    <td><?php echo $issue_type; ?></td>

                                    <td><?php echo $remarks; ?></td>
                                    <td><?php echo $added_by; ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<script src="../assets/excel/js/FileSaver.min.js" type="text/javascript"></script>
<script src="../assets/excel/js/bootstrap.min_1.js" type="text/javascript"></script>
<script src="../assets/excel/js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="../assets/excel/js/tableexport.min.js" type="text/javascript"></script>
<script>
$('#NormalIssue-Chipset').tableExport();
</script>

