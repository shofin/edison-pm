<?php
$message = '';


$query_result = $obj_meetings->select_all_meetings();
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header text-center text-success"></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading text-center lead">
                All Meetings Information Goes Here
            </div>
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SL NO</th>
                            <th>Date/Time</th>
                            <th>Title/Subject</th>
                            <th>Short Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($all_meetings = mysqli_fetch_assoc($query_result)) {
                            extract($all_meetings);
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $date; ?></td>
                                <td><?php echo $subject; ?></td>
                                <td><?php echo $short_description; ?></td>
                                <td class="center">
                                    <a href="view_meeting_minutes.php?id=<?php echo $meetings_id; ?>" class="btn btn-success" title="View">
                                        <span class="glyphicon glyphicon-zoom-in"></span>
                                    </a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>