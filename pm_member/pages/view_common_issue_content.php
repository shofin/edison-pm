<?php
$common_issue_id = $_GET['id'];
$query_result = $obj_common_issue->select_common_issue_by_id($common_issue_id);
$common_issue_by_id = mysqli_fetch_assoc($query_result);
extract($common_issue_by_id);
?>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading text-center lead">
                Common Issue Details<br>
                <a href="common_issues.php" class="btn btn-primary" title="Back" >
                    <span class="glyphicon glyphicon-backward"></span>
                </a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example"> 
                    <tr>
                        <td>Project Name</td>
                        <td><?php echo $project_name; ?></td>
                    </tr>
                    <tr>
                        <td>Project Owner Name</td>
                        <td><?php
                            echo $project_owner_name;
                            ?></td>
                    </tr>
                    <tr>
                        <td>ODM Name</td>
                        <td><?php echo $odm_name; ?></td>
                    </tr>
                    <tr>
                        <td>Chipset</td>
                        <td><?php echo $chipset; ?></td>
                    </tr>

                    <tr>
                        <td>Title</td>
                        <td><?php echo $subject; ?></td>
                    </tr>
                    <tr>
                        <td>Issue Details</td>
                        <td><?php echo $issue_details; ?></td>
                    </tr>
                    <tr>
                        <td>Remarks</td>
                        <td><?php echo $remarks; ?></td>
                    </tr>
                    <tr>
                        <td>Posted By</td>
                        <td><?php echo $added_by; ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
