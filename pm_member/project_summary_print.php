<?php
require 'pdf/fpdf.php';
require '../classes/project_summary.php';
$obj_project_summary = new Project_summary();


$project_summary_id = $_GET['id'];

$query_result = $obj_project_summary->details_project_summary_info_by_id($project_summary_id);

while ($all_project_summary_info =mysqli_fetch_assoc($query_result)){
    extract($all_project_summary_info);




$obj_pdf = new FPDF();
//var_dump(get_class_methods($obj_pdf));
$obj_pdf-> AddPage();
$obj_pdf->Image('../assets/images/project/edison_group.jpg', 10,10, 20, 20);
$obj_pdf->Image('../assets/images/project/symphony.jpg', 170,10, 30, 20);
$obj_pdf->Cell(0, 20,'', 0,1, "");
//$obj_pdf-> SetTopMargin(2);
$obj_pdf->SetFont('Arial', 'I', 14);
$obj_pdf->Cell(0, 10, 'Edison Product Management Team', 1,1, "C");

$obj_pdf->SetFont('courier', 'B', 12);
$obj_pdf->Cell(0, 10, 'Project Details', 0,1, "C");

$obj_pdf->SetFont('Arial', 'B', 8);
$obj_pdf->Cell(20, 8, 'ID: '.$project_summary_id, 1,0, "L");
$obj_pdf->Cell(60, 8, 'Project Owner: '.$project_owner_name, 1,0, "L");
$obj_pdf->Cell(40, 8, 'Project Name: '.$project_name, 1,0, "C");
$obj_pdf->Cell(70, 8, 'ODM Name: '.$odm_name, 1,1, "C");

$obj_pdf->Cell(0, 5,'', 0,1, "");

$obj_pdf->SetFont('Arial', '', 8);

$obj_pdf->Cell(60, 8, 'Developer: '.$developer_name, 1,0, "L");
$obj_pdf->Cell(130, 8, 'Tester: '.$tester_1_name.', '.$tester_2_name.', '.$tester_3_name.', '.$tester_4_name, 1,1, "L");

$obj_pdf->Cell(0, 5,'', 0,1, "");

$obj_pdf->Cell(50, 5, 'Software Version: '.'HW'.$hardware_version.'_V'.$current_soft_version, 0,0, "L");
$obj_pdf->Cell(45, 5, 'Total Issue: '.$total_issue, 0,0, "L");
$obj_pdf->Cell(45, 5, 'Resolve Issue: '.$resolve_issue, 0,0, "L");
$obj_pdf->Cell(50, 5, 'Open Issue: '.$open_issue, 0,1, "L");

$obj_pdf->Cell(0, 5,'', 0,1, "");

$obj_pdf->Cell(50, 5, 'Normal Issue: '.$normal_issue, 0,0, "L");
$obj_pdf->Cell(50, 5, 'After Sales Issue: '.$after_sales_issue, 0,0, "L");
$obj_pdf->Cell(90, 5, 'After Sales Resolve Issue: '.$after_sales_resolve_issue, 0,1, "L");

$obj_pdf->Cell(0, 5,'', 0,1, "");

$obj_pdf->Cell(70, 5, 'Confirm Soft. Version: V'.$confirm_soft_version, 0,0, "L");
$obj_pdf->Cell(60, 5, 'Confirmation Date: '.$confirmation_date, 0,0, "L");
$obj_pdf->Cell(60, 5, 'Shipment Date: '.$shipment_date, 0,1, "L");

$obj_pdf->Cell(0, 10,'', 0,1, "");

//$obj_pdf->Cell(0, 10, 'Any Difficulties? '.$any_difficulties, 0,1, "L");

//$obj_pdf->SetFont('', 'I', 10);
//$obj_pdf->Write(10, 'ghv ukhjytgvk kuy');

$obj_pdf->Cell(0, 10,'', 0,1, "");
$obj_pdf->Image($models_image, 165,110, 30, 40);

$obj_pdf->SetFont('arial', 'B', 8);
$obj_pdf->Cell(130, 5 , 'Project Owner Signature', 0,0, "L");
$obj_pdf->Cell(50, 5, 'Model Image', 0,0, "L");

$obj_pdf->Output('','Reports On_'.$project_name.'.pdf');


}