<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a href="pm_master.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Project <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="all_project_summary.php"> All Project Summary </a>
                    </li>
<!--                    <li>
                        <a href="manage_project_summary.php"> Manage Project Summary </a>
                    </li>-->
                </ul>
            </li>
            
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Meetings <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="meetings.php">All Meetings List</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Issues <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="common_issues.php">Common Issues</a>
                    </li>
                    <li>
                        <a href="normal_issues.php">Normal Issues</a>
                    </li>
                </ul>
            </li>
            
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>