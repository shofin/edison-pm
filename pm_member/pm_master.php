<?php
ob_start();
session_start();
require '../classes/login_signup.php';

$obj_login_signup = new Login_signup();


require '../classes/project_summary.php';
$obj_project_summary = new Project_summary();

require '../classes/meetings.php';
$obj_meetings = new Meetings();

require '../classes/common_issue.php';
$obj_common_issue = new Common_issue();

require '../classes/normal_issue.php';
$obj_normal_issue = new Normal_issue();

if ($_SESSION['pm_member'] == 0) {
    header('Location: ../user_master.php');
}


if (isset($_GET['status'])) {
    if ($_GET['status'] == 'logout') {
        $obj_login_signup->pm_user_logout();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Edison PM Member</title>
        <!-- Bootstrap Core CSS -->
        <link href="../assets/backend/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- MetisMenu CSS -->
        <link href="../assets/backend/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
        <link href="../assets/backend/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
        <!-- DataTables Responsive CSS -->
        <link href="../assets/backend/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../assets/backend/dist/css/sb-admin-2.css" rel="stylesheet">
        <!-- Morris Charts CSS -->
        <link href="../assets/backend/vendor/morrisjs/morris.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="../assets/backend/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <script>
            function check_delete_status() {
                var check = confirm('Are you sure to delete this !');
                if (check) {
                    return true;
                } else {
                    return false;
                }
            }
        </script>
    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">Edison PM Member</a>
                </div>
                <!-- /.navbar-header -->
                <?php include './includes/top_menu.php'; ?>
                <!-- /.navbar-top-links -->
                <?php include './includes/sidebar.php'; ?>
                <!-- /.navbar-static-side -->
            </nav>  

            <div id="page-wrapper">
                <?php
                if (isset($pages)) {
                    if ($pages == 'all_project_summary') {
                        include './pages/all_project_summary_content.php';
                    } 
                    
                    else if ($pages == 'view_project_details') {
                        include './pages/view_project_details_content.php';
                    } 
                    else if ($pages == 'mettings') {
                        include './pages/meetings_content.php';
                    } 
                    else if ($pages == 'view_meetings') {
                        include './pages/view_meetings_content.php';
                    } 
                    else if ($pages == 'common_issues') {
                        include './pages/common_issues_content.php';
                    } 
                    else if ($pages == 'view_common_issue') {
                        include './pages/view_common_issue_content.php';
                    } 
                    else if ($pages == 'search_common_issues') {
                        include './pages/search_common_issues_content.php';
                    } 
                    else if ($pages == 'normal_issues') {
                        include './pages/normal_issues_content.php';
                    } 
                    else if ($pages == 'view_normal_issues') {
                        include './pages/view_normal_issues_content.php';
                    } 
                    else if ($pages == 'search_normal_issues') {
                        include './pages/search_normal_issues_content.php';
                    } 
                } else {
                    include './pages/home_content.php';
                }
                ?>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- jQuery -->
        <script src="../assets/backend/vendor/jquery/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="../assets/backend/vendor/bootstrap/js/bootstrap.min.js"></script>
        <!-- Metis Menu Plugin JavaScript -->
        <script src="../assets/backend/vendor/metisMenu/metisMenu.min.js"></script>
        <!-- Morris Charts JavaScript -->
        <script src="../assets/backend/vendor/raphael/raphael.min.js"></script>
        <script src="../assets/backend/vendor/morrisjs/morris.min.js"></script>
        <script src="../assets/backend/data/morris-data.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="../assets/backend/vendor/datatables/js/jquery.dataTables.min.js"></script>
        <script src="../assets/backend/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
        <script src="../assets/backend/vendor/datatables-responsive/dataTables.responsive.js"></script>

        <script src="../assets/backend/dist/js/sb-admin-2.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').DataTable({
                    responsive: true
                });
            });

//             $(document).ready(function () {
//                alert('hai');
//            });


        </script>
    </body>
</html>
