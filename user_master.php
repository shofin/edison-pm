<?php
ob_start();
session_start();
require 'classes/login_signup.php';
$obj_login_signup = new Login_signup();

require 'classes/slider.php';
$obj_slider = new Slider();

require 'classes/flagship.php';
$obj_flagship = new Flagship();

require 'classes/about_us.php';
$obj_about_us = new About_us();

require 'classes/team_leader.php';
$obj_team_leader = new Team_leader();

require 'classes/pm_member.php';
$obj_pm_member = new Pm_member();

require 'classes/meetings.php';
$obj_meetings = new Meetings();

require 'classes/project_summary.php';
$obj_project_summary = new Project_summary();


if ($_SESSION['user_id'] == NULL) {
    header('Location: index.php');
}

if (isset($_GET['status'])) {
    if ($_GET['status'] == 'logout') {
        $obj_login_signup->user_logout();
    }
}
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Edison Product Management Team</title>
        <!-- core CSS -->
        <link href="assets/frontend/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/frontend/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/frontend/css/animate.min.css" rel="stylesheet">
        <link href="assets/frontend/css/owl.carousel.css" rel="stylesheet">
        <link href="assets/frontend/css/owl.transitions.css" rel="stylesheet">
        <link href="assets/frontend/css/prettyPhoto.css" rel="stylesheet">
        <link href="assets/frontend/css/main.css" rel="stylesheet">
        <link href="assets/frontend/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/frontend/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/frontend/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/frontend/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/frontend/images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <body id="home" class="homepage">

        <?php include 'includes/header.php'; ?>
        <?php include 'includes/slider.php'; ?>
        <?php include 'includes/policy.php'; ?>
        <?php
        include 'includes/flagship_model.php';
//        if ($_SESSION['pm_member'] == 1) {
//            include 'includes/flagship_model.php';
//        }
        ?>


        <?php include 'includes/comment_linker.php'; ?>
        <?php include 'includes/services.php'; ?>
        <?php include 'includes/projects.php'; ?>
        <?php include 'includes/about_us.php'; ?>
        <?php 
                if ($_SESSION['pm_member'] == 1) {
            include 'includes/blogs.php';
        }
        ?>

        <section id="get-in-touch">
            <div class="container">
                <div class="section-header">
                    <h2 class="section-title text-center wow fadeInDown">Get in Touch</h2>
                    <p class="text-center wow fadeInDown">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
                </div>
            </div>
        </section><!--/#get-in-touch-->

        <?php include 'includes/footer.php'; ?>
<!--    <section id="contact">
        <div id="google-map" style="height:650px" data-latitude="52.365629" data-longitude="4.871331"></div>
        <div class="container-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-8">
                        <div class="contact-form">
                            <h3>Contact Info</h3>

                            <address>
                              <strong>Twitter, Inc.</strong><br>
                              795 Folsom Ave, Suite 600<br>
                              San Francisco, CA 94107<br>
                              <abbr title="Phone">P:</abbr> (123) 456-7890
                            </address>

                            <form id="main-contact-form" name="contact-form" method="post" action="#">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="Name" required>
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="subject" class="form-control" placeholder="Subject" required>
                                </div>
                                <div class="form-group">
                                    <textarea name="message" class="form-control" rows="8" placeholder="Message" required></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">Send Message</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>/#bottom-->

        <!--/#footer-->

        <script src="assets/frontend/js/jquery.js"></script>
        <script src="assets/frontend/js/bootstrap.min.js"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="assets/frontend/js/owl.carousel.min.js"></script>
        <script src="assets/frontend/js/mousescroll.js"></script>
        <script src="assets/frontend/js/smoothscroll.js"></script>
        <script src="assets/frontend/js/jquery.prettyPhoto.js"></script>
        <script src="assets/frontend/js/jquery.isotope.min.js"></script>
        <script src="assets/frontend/js/jquery.inview.min.js"></script>
        <script src="assets/frontend/js/wow.min.js"></script>
        <script src="assets/frontend/js/main.js"></script>
    </body>
</html>