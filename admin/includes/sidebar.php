<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a href="admin_master.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Project Control Panel <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="add_project_summary.php"> Add Project Summary </a>
                    </li>
                    <li>
                        <a href="manage_project_summary.php"> Manage Project Summary </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Model Control Panel <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="add_model.php"> Add Model </a>
                    </li>
                    <li>
                        <a href="manage_model.php"> Manage Models </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> News Control Panel <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="meeting_minutes.php"> New Meeting </a>
                    </li>
                    <li>
                        <a href="manage_meetings.php"> Manage Meeting </a>
                    </li>
                    <li>
                        <a href="add_news_feed.php"> Add News Feed </a>
                    </li>
                    <li>
                        <a href="manage_news_feed.php"> Manage News Feed </a>
                    </li>
                    <li>
                        <a href="add_knowledge_share.php">Knowledge Share </a>
                    </li>
                    <li>
                        <a href="manage_knowledge_share.php">Manage Knowledge Share </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Manage Issues <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="add_common_issue.php"> New Common Issue </a>
                    </li>
                    <li>
                        <a href="manage_common_issue.php"> Manage Common Issue </a>
                    </li>
                    <li>
                        <a href="add_normal_issue.php"> New Normal Issue </a>
                    </li>
                    <li>
                        <a href="manage_normal_issue.php"> Manage Normal Issue </a>
                    </li>
                </ul>
            </li>            
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Project Owner Info <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="add_project_owner.php"> New Project Owner </a>
                    </li>
                    <li>
                        <a href="manage_project_owner.php"> Manage Project Owner </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Tester Info <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="add_tester.php"> New Tester Form </a>
                    </li>
                    <li>
                        <a href="manage_tester.php"> Manage Tester </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> File Upload <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="add_files.php"> Add Files </a>
                    </li>
                    <li>
                        <a href="manage_files.php"> Manage Files </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> ODM Info <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="add_odm.php"> Add ODM </a>
                    </li>
                    <li>
                        <a href="manage_odm.php"> Manage ODM </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Project Info <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="add_project.php"> Add Project </a>
                    </li>
                    <li>
                        <a href="manage_project.php"> Manage Project </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Developer Info <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="add_developer.php"> Add Developer </a>
                    </li>
                    <li>
                        <a href="manage_developer.php"> Manage Developer </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> User Control Panel <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="add_user.php"> Add User </a>
                    </li>
                    <li>
                        <a href="manage_user.php"> Manage User </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i>Slider Info <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="main_slider.php"> Add Slider </a>
                    </li>
                    <li>
                        <a href="manage_slider.php"> Manage Slider </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i>Flagship Models <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="add_flagship_models.php"> Add Flagship Models </a>
                    </li>
                    <li>
                        <a href="manage_flagship_models.php"> Manage Flagship Models </a>
                    </li>
                </ul>
            </li>
            <!--            <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Manufacturer Info <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="add_manufacturer.php"> Add Manufacturer </a>
                                </li>
                                <li>
                                    <a href="manage_manufacturer.php"> Manage Manufacturer </a>
                                </li>
                            </ul>
                        </li>-->
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i>ABOUT US<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="add_about_us.php"> Add About Us </a>
                    </li>
                    <li>
                        <a href="manage_about_us.php"><div class="fa fa">Manage About Us</div> </a>
                    </li>

                    <li>
                        <a href="add_team_leader.php">Add Team Leader</a>
                    </li>
                    <li>
                        <a href="manage_team_leader.php"> <div class="fa fa">Manage Team Leader</div></a>
                    </li>

                </ul>
            </li>
            <!--            <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Product Info <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="add_product.php"> Add Product </a>
                                </li>
                                <li>
                                    <a href="manage_product.php"> Manage Product </a>
                                </li>
                            </ul>
                        </li>-->



            <!--            <li>
                            <a href="tables.html"><i class="fa fa-table fa-fw"></i> Tables</a>
                        </li>
                        <li>
                            <a href="forms.html"><i class="fa fa-edit fa-fw"></i> Forms</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="panels-wells.html">Panels and Wells</a>
                                </li>
                                <li>
                                    <a href="buttons.html">Buttons</a>
                                </li>
                                <li>
                                    <a href="notifications.html">Notifications</a>
                                </li>
                                <li>
                                    <a href="typography.html">Typography</a>
                                </li>
                                <li>
                                    <a href="icons.html"> Icons</a>
                                </li>
                                <li>
                                    <a href="grid.html">Grid</a>
                                </li>
                            </ul>
                             /.nav-second-level 
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">Second Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Second Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Third Level <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                    </ul>
                                     /.nav-third-level 
                                </li>
                            </ul>
                             /.nav-second-level 
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="blank.html">Blank Page</a>
                                </li>
                                <li>
                                    <a href="login.html">Login Page</a>
                                </li>
                            </ul>
                             /.nav-second-level 
                        </li>-->
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>