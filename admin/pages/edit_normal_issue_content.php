<?php
$query_result = $obj_odm->select_all_odm_info();
$query_project = $obj_project->select_all_project_info();

$normal_issue_id = $_GET['id']; 

$query_normal_issue = $obj_normal_issue->select_normal_issue_by_id($normal_issue_id);
$normal_issue = mysqli_fetch_assoc($query_normal_issue);
extract($normal_issue);

if (isset($_POST['btn'])) {
    $obj_normal_issue->update_normal_issue_by_id($_POST);
}

?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p class="text-center text-success lead">Edit Normal Issue Form</p>
                
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="" name="normal_issue" method="post">
                    <div class="form-group">
                        <label class="control-label col-lg-3">Project Name</label>
                        <div class="col-lg-9">
                            <input type="hidden" name="normal_issue_id" value="<?php echo $normal_issue_id?>"/>
                            <select class="form-control" name="project_id">
                                <option> --- Select Project Name --- </option>
                               <?php  while ( $all_project_info = mysqli_fetch_assoc($query_project))  { ?>
                                <option value="<?php echo $all_project_info['project_id']; ?>"><?php echo $all_project_info['project_name']; ?></option>
                               <?php };?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">ODM Name</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="odm_id">
                                
                                <option> --- Select ODM Name --- </option>
                               <?php  while ( $all_odm_info = mysqli_fetch_assoc($query_result))  { ?>
                                <option value="<?php echo $all_odm_info['odm_id']; ?>"><?php echo $all_odm_info['odm_name']; ?></option>
                               <?php };?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Chipset</label>
                        <div class="col-lg-9">
                            <input type="text" name="chipset" value="<?php echo $chipset;?>"class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Title of Issue</label>
                        <div class="col-lg-9">
                            <input type="text" name="title_of_issue" value="<?php echo $title_of_issue;?>" class="form-control"/>
                        </div>
                    </div>
                   <div class="form-group">
                        <label class="control-label col-lg-3">Issue Type</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="issue_type">
                                <option> --- Select Issue Type --- </option>
                                <option value="1">Chipset</option>
                                <option value="2">OS</option>
                            </select>
                        </div>
                   </div>
                    
                    <div class="form-group">
                        <label class="control-label col-lg-3">Platform</label>
                        <div class="col-lg-9">
                            <input type="text" name="platform" value="<?php echo $platform;?>" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Remarks</label>
                        <div class="col-lg-9">
                            <textarea name="remarks" class="form-control" rows="6"><?php echo $title_of_issue;?></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <input type="submit" name="btn" value="Update Normal Issue" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    document.forms['normal_issue'].elements['project_id'].value='<?php echo $project_id; ?>';
    document.forms['normal_issue'].elements['odm_id'].value='<?php echo $odm_id; ?>';
    document.forms['normal_issue'].elements['issue_type'].value='<?php echo $issue_type; ?>';
</script>