<?php
$meetings_id = $_GET['id'];
$query_result = $obj_meetings->view_meetings_by_id($meetings_id);


?>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading text-center lead">
                Meetings Information Goes Here<br>
                <a href="manage_meetings.php" class="btn btn-primary" title="Back" >
                    <span class="glyphicon glyphicon-backward"></span>
                </a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <?php
                    $meetings_info_by_id = mysqli_fetch_assoc($query_result);
                    extract($meetings_info_by_id);
                    ?>    
                    <tr>
                        <td>Meeting No.</td>
                        <td><?php echo $meetings_id; ?></td>
                    </tr>
                    <tr>
                        <td>Date/Time</td>
                        <td><?php
                            echo $date;
                            ?></td>
                    </tr>
                    <tr>
                        <td>Duration</td>
                        <td><?php echo $duration; ?></td>
                    </tr>
                    <tr>
                        <td>Chair</td>
                        <td><?php echo $chair; ?></td>
                    </tr>

                    <tr>
                        <td>Title/Subject</td>
                        <td><?php echo $subject; ?></td>
                    </tr>

                    <tr>
                        <td>Short Description</td>
                        <td><?php echo $short_description; ?></td>
                    </tr>
                    
                    <tr>
                        <td>Remarks</td>
                        <td><?php echo $remarks; ?></td>
                    </tr>
                    <tr>
                        <td>Report File</td>
                        <td><a href="<?php echo $meeting_minutes_file; ?> "target=”_blank” ><?php echo 'report_'.$subject.'_'.$date;?></a></td>
                    </tr>

                </table>
                <table width="100%" class="table table-striped table-bordered table-hover table-condensed center" id="dataTables-example">
                    <thead class="center">
                        <tr>
                            <th>Area</th>
                            <th>Decision</th>
                            <th>Responsible</th>
                            <th>Target Date</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td><?php echo $area_1; ?></td>
                            <td><?php echo $decision_1; ?></td>
                            <td><?php echo $responsible_1; ?></td>
                            <td><?php echo $target_date_1; ?></td>

                        </tr>
                        <tr>

                            <td><?php echo $area_2; ?></td>
                            <td><?php echo $decision_2; ?></td>
                            <td><?php echo $responsible_2; ?></td>
                            <td><?php echo $target_date_2; ?></td>

                        </tr>
                        
                        <tr>

                            <td><?php echo $area_3; ?></td>
                            <td><?php echo $decision_3; ?></td>
                            <td><?php echo $responsible_3; ?></td>
                            <td><?php echo $target_date_3; ?></td>

                        </tr>
                    </tbody>

                </table>
            </div>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

