<?php
$flagship_id = $_GET['id']; 
$query_result = $obj_flagship->select_flagship_info_by_id($flagship_id);
 $flagship_info = mysqli_fetch_assoc($query_result);
 extract($flagship_info);

   if(isset($_POST['btn'])) {
       $obj_flagship->update_flagship_info_by_id($_POST);
   }
   
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p class="text-center text-success lead">Flagship Models Goes Here</p>
                <h3 class="text-center text-success lead"></h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="" method="post" name="edit_flagship_form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-lg-3">Flagship Models Title</label>
                        <div class="col-lg-9">
                            <input type="text" name="flagship_title" value="<?php echo $flagship_title; ?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Flagship Models Name</label>
                        <div class="col-lg-9">
                            <input type="text" name="flagship_model_name" value="<?php echo $flagship_model_name; ?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Short Description</label>
                        <div class="col-lg-9">
                            <textarea name="short_description" class="form-control" rows="6"><?php echo $short_description; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Key Feature</label>
                        <div class="col-lg-9">
                            <textarea name="key_feature" class="form-control" rows="6"><?php echo $key_feature; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Flagship Models Image</label>
                        <div class="col-lg-9">
                            <img src="<?php echo $flagship_image; ?>" alt="" height="100" width="100">
                            <input type="file"  name="flagship_image">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Flagship Models Youtube Link</label>
                        <div class="col-lg-9">
                            <input type="text" name="flagship_video" class="form-control" required>                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Publication Status</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="publication_status">
                                <option> --- Select Publication Status --- </option>
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <input type="submit" name="btn" value="Update Flagship Info" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    document.forms['edit_flagship_form'].elements['flagship_video'].value='<?php echo $flagship_video; ?>';
    document.forms['edit_flagship_form'].elements['publication_status'].value='<?php echo $publication_status; ?>';
</script>