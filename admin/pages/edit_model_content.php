<?php
$message='';

$query_project_owner = $obj_project_owner->select_all_project_owner_info();
$query_odm = $obj_odm->select_all_odm_info();

$model_id = $_GET['id'];

$query_result = $obj_models->edit_model_info_by_id($model_id);
$model_info = mysqli_fetch_assoc($query_result);
extract($model_info);

if (isset($_POST['btn'])) {
    $obj_models->update_model_info_by_id($_POST);
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p class="text-center text-success lead">Edit Model Info </p>
                <h3 class="text-center text-success lead"><?php echo $message; ?></h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="" name="model_info"method="post">
                    <div class="form-group">
                        <label class="control-label col-lg-3">Model Name</label>
                        <div class="col-lg-9">
                            <input type="text" name="model_name" value="<?php echo $model_name;?>" class="form-control" required>
                        <input type="hidden" name="model_id" value="<?php echo $model_id; ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">phone_type</label>
                        <div class="col-lg-9">
                            <input type="text" name="phone_type" value="<?php echo $phone_type;?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Project Owner Name</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="project_owner_id">
                                
                                <option> --- Select Project Owner --- </option>
                               <?php  while ( $all_project_owner = mysqli_fetch_assoc($query_project_owner))  { ?>
                                <option value="<?php echo $all_project_owner['project_owner_id']; ?>"><?php echo $all_project_owner['project_owner_name']; ?></option>
                               <?php };?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3"> ODM Name</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="odm_id">
                                
                                <option> --- Select ODM --- </option>
                               <?php  while ( $all_odm = mysqli_fetch_assoc($query_odm))  { ?>
                                <option value="<?php echo $all_odm['odm_id']; ?>"><?php echo $all_odm['odm_name']; ?></option>
                               <?php };?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Spec and NPD</label>
                        <div class="col-lg-9">
                            <input type="text" name="spec_and_npd" value="<?php echo $spec_and_npd;?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">po_date</label>
                        <div class="col-lg-9">
                            <input type="date" name="po_date" value="<?php echo $po_date;?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Launching Year</label>
                        <div class="col-lg-9">
                            <input type="text" name="launching_year" value="<?php echo $launching_year;?>" class="form-control">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <input type="submit" name="btn" value="Update model Info" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    document.forms['model_info'].elements['project_owner_id'].value='<?php echo $project_owner_id; ?>';
    document.forms['model_info'].elements['odm_id'].value='<?php echo $odm_id; ?>';
</script>