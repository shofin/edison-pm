<?php
$team_leader_id = $_GET['id']; 
$query_result = $obj_team_leader->select_team_leader_info_by_id($team_leader_id);
 $team_leader_info = mysqli_fetch_assoc($query_result);
 extract($team_leader_info);

   if(isset($_POST['btn'])) {
       $obj_team_leader->update_team_leader_info_by_id($_POST);
   }
   
?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p class="text-center text-success lead">Update Team Leader Form</p>
                <h3 class="text-center text-success lead"></h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="" method="post" name="edit_team_leader_form"enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-lg-3">Team Leader Name</label>
                        <div class="col-lg-9">
                            <input type="text" name="team_leader_name" value="<?php echo $team_leader_name ;?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3"> Nick Name</label>
                        <div class="col-lg-9">
                            <input type="text" name="nick_name"  value="<?php echo $nick_name ;?>" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Team Leader Designation</label>
                        <div class="col-lg-9">
                            <input type="text" name="team_leader_designation" value="<?php echo $team_leader_designation ;?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Team Leader Employee ID</label>
                        <div class="col-lg-9">
                            <input type="number" name="employe_id" value="<?php echo $employe_id ;?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">career Short Description</label>
                        <div class="col-lg-9">
                            <textarea name="career_short_description" class="form-control" rows="6"><?php echo $career_short_description ;?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Team Leader Image</label>
                        <div class="col-lg-9">
                            <img src="<?php echo $team_leader_image; ?>" alt="" height="100" width="100">
                            <input type="file"  name="team_leader_image">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Publication Status</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="publication_status">
                                <option> --- Select Publication Status --- </option>
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <input type="submit" name="btn" value="Save Team Leader" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    document.forms['edit_team_leader_form'].elements['publication_status'].value='<?php echo $publication_status; ?>';
</script>