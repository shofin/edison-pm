<?php
$slider_id = $_GET['id'];
$query_result = $obj_slider->select_slider_info_by_id($slider_id);
$slider_info = mysqli_fetch_assoc($query_result);
extract($slider_info);

if (isset($_POST['btn'])) {
    $obj_slider->update_slider_info_by_id($_POST);
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p class="text-center text-success lead">Edit Slider Information</p>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="" method="post" name="edit_category_form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-lg-3">Slider Name</label>
                        <div class="col-lg-9">
                            <input type="text" name="slider_name" value="<?php echo $slider_name; ?>" class="form-control" required>
                            <input type="hidden" name="slider_id" value="<?php echo $slider_id; ?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Slider Title</label>
                        <div class="col-lg-9">
                            <input type="text" name="slider_title" value="<?php echo $slider_title; ?>" class="form-control" required>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Slider Description</label>
                        <div class="col-lg-9">
                            <textarea name="slider_description" class="form-control" rows="6"><?php echo $slider_description; ?></textarea>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Slider Image</label>
                        <div class="col-lg-9">
                            <img src="<?php echo $slider_info['slider_image']; ?>" alt="" height="100" width="100">
                            <input type="file"  name="slider_image">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Publication Status</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="publication_status">
                                <option> --- Select Publication Status --- </option>
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <input type="submit" name="btn" value="Update Slider Info" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    document.forms['edit_category_form'].elements['publication_status'].value = '<?php echo $publication_status; ?>';
</script>