<?php
$message = '';

if (isset($_GET['status'])) {
    $common_issue_id = $_GET['id'];

    if ($_GET['status'] == 'delete') {
        $obj_common_issue->delete_common_issue_by_id($common_issue_id);
    }
}

$query_result = $obj_common_issue->select_all_common_issue_info();
?>
<div class="panel-body">
    <form class="form-horizontal" action="search_common_issue.php" method="post">
        <div class="form-group">
            <div class="col-lg-3">
                <input type="text" name="search" placeholder="Search By Chipset" required="" class="form-control"/>   
            </div>
            <div class="col-lg-2">
                <input type="submit" name="submit" value="Search" class="form-control"/>
            </div>
        </div>
    </form>
</div>
<div class="row">  
        <h3 class="page-header text-center text-success">
            <?php echo $message; ?>
            <?php
            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            }
            ?>
        </h3>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading text-center lead">
                All Issues Information Goes Here
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SL NO</th>
                            <th>Project Name</th>
                            <th>Project Owner</th>
                            <th>ODM Name</th>
                            <th>Chipset</th>
                            <th>Subject </th>
                            <th>Issue Details</th>
                            <th>remarks</th>
                            <th>Added By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($all_common_issue = mysqli_fetch_assoc($query_result)) {
                            extract($all_common_issue);
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $project_name; ?></td>
                                <td><?php echo $project_owner_name; ?></td>
                                <td><?php echo $odm_name; ?></td>
                                <td><?php echo $chipset; ?></td>
                                <td><?php echo $subject; ?></td>
                                <td><?php echo $issue_details; ?></td>

                                <td><?php echo $remarks; ?></td>
                                <td><?php echo $added_by; ?></td>
                                <td class="center">
                                    <a href="edit_common_issue.php?id=<?php echo $common_issue_id; ?>" class="btn btn-success" title="Edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a href="?status=delete&&id=<?php echo $common_issue_id; ?>" class="btn btn-danger" title="Delete" onclick="return check_delete_status();">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>