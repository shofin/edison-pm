<?php
$message = '';

if (isset($_GET['status'])) {
    $manpower_id = $_GET['id'];
    if ($_GET['status'] == 'delete') {
        $message = $obj_manpower->delete_manpower_info_by_id($manpower_id);
    }
}

$query_result = $obj_models->select_all_models_info();
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header text-center text-success">
            <?php echo $message; ?>
            <?php
            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            }
            ?>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading text-center lead">
                All Models Information Goes Here
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>ID Model</th>
                            <th>Model Name</th>
                            <th>Phone Type</th>
                            <th>Owner Name</th>
                            <th>Supplier Name</th>
                            <th>Spec and NPD</th>
                            <th>PO Date</th>
                            <th>Launching Year</th>

                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($all_models_info = mysqli_fetch_assoc($query_result)) {
                            extract($all_models_info);
                            ?>
                            <tr class="odd gradeX">
                                <td> <?php echo $i; ?></td>
                                <td><?php echo $model_name; ?></td>
                                <td><?php echo $phone_type; ?></td>
                                <td><?php echo $project_owner_name; ?></td>

                                <td> <?php echo $odm_name; ?> </td>
                                <td> <?php echo $spec_and_npd; ?> </td>
                                <td> <?php echo $po_date; ?> </td>
                                <td> <?php echo $launching_year; ?> </td>

                                <td class="center">

                                    <a href="edit_model.php?id=<?php echo $model_id; ?>" class="btn btn-success" title="Edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a href="?status=delete&&id=<?php echo $model_id; ?>" class="btn btn-danger" title="Delete" onclick="return check_delete_status();">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table
                
                <!--single excel file download-->
<!--                <div>
                    <form action="export_report.php" method="post">
                        <input type="submit" name="export_excel" class="btn btn-success"value="Download-Excel"/>
                    </form>
                </div>-->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<script src="../assets/excel/js/FileSaver.min.js" type="text/javascript"></script>
<script src="../assets/excel/js/bootstrap.min_1.js" type="text/javascript"></script>
<script src="../assets/excel/js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="../assets/excel/js/tableexport.min.js" type="text/javascript"></script>
<script>
$('#dataTables-example').tableExport();
</script>