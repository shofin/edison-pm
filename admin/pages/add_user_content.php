<?php
$message = '';

if (isset($_POST['btn'])) {
    $message = $obj_user->save_add_user_info($_POST);
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p class="text-center text-success lead">Add Users Form</p>
                <h3 class="text-center text-success lead"><?php echo $message; ?></h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-lg-3">User Name</label>
                        <div class="col-lg-9">
                            <input type="text" name="user_name" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3"> Employee ID</label>
                        <div class="col-lg-9">
                            <input type="number" name="employe_id" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Phone Number</label>
                        <div class="col-lg-9">
                            <input type="number" name="phone_number" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">E-mail Address</label>
                        <div class="col-lg-9">
                            <input type="email" name="email_address" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Password</label>
                        <div class="col-lg-9">
                            <input type="password" name="password" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Confirm Password</label>
                        <div class="col-lg-9">
                            <input type="password" name="confirm_password" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">PIN Number</label>
                        <div class="col-lg-9">
                            <input type="password" name="pin_number" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Career Short Description</label>
                        <div class="col-lg-9">
                            <textarea name="career_short_description" class="form-control" rows="6"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">User Image</label>
                        <div class="col-lg-9">
                            <input type="file"  name="user_image">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <input type="submit" name="btn" value="Save User" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>