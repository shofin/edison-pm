<?php
$message = '';

if (isset($_GET['status'])) {
    $knowledge_share_id = $_GET['id'];
    if ($_GET['status'] == 'unpublished') {
        $message = $obj_meetings->unpublished_knowledge_share_by_id($knowledge_share_id);
    } else if ($_GET['status'] == 'published') {
        $message = $obj_meetings->published_knowledge_share_by_id($knowledge_share_id);
    } else if ($_GET['status'] == 'delete') {
        $message = $obj_meetings->delete_knowledge_share_by_id($knowledge_share_id);
    }
}
$query_result = $obj_meetings->select_all_knowledge_share_info();
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header text-center text-success">
            <?php echo $message; ?>
            <?php
            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            }
            ?>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading text-center lead">
                All Knowledge Share Info Goes Here
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SL NO</th>
                            <th>Date/Time</th>
                            <th>Publisher</th>
                            <th>Title</th>
                            <th>Contents</th>
                            <th>File</th>
                            <th>Publication Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($all_knowledge_share = mysqli_fetch_assoc($query_result)) {
                            extract($all_knowledge_share);
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $date_time; ?></td>
                                <td><?php echo $publisher_name; ?></td>
                                <td><?php echo $knowledge_share_title; ?></td>
                                <td><?php echo $knowledge_share_contents; ?></td>
                                <td><a href="<?php echo $knowledge_share_file; ?>">Download</a></td>
                                <td><?php
                                    if ($publication_status == 1) {
                                        echo 'Published';
                                    } else {
                                        echo 'Unpublished';
                                    }
                                    ?></td>
                                <td class="center">
                                    <?php if ($publication_status == 1) { ?>
                                        <a href="?status=unpublished&&id=<?php echo $knowledge_share_id; ?>" class="btn btn-primary" title="Unpublished">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    <?php } else { ?>
                                        <a href="?status=published&&id=<?php echo $knowledge_share_id; ?>" class="btn btn-danger" title="Published">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    <?php } ?>
                                    <a href="#?id=<?php echo $news_feed_id; ?>" class="btn btn-success" title="Edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a href="?status=delete&&id=<?php echo $knowledge_share_id; ?>" class="btn btn-danger" title="Delete" onclick="return check_delete_status();">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                            <?php $i++;
                        }
                        ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>