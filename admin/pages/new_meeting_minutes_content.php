<?php
$message = '';

if (isset($_POST['btn'])) {
    $message = $obj_meetings->save_meeting_minutes_info($_POST);
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p class="text-center text-success lead">New Meeting Minutes Form</p>
                <h3 id="test" class="text-center text-success lead"><?php echo $message; ?></h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-lg-3">Meeting Minutes Image</label>
                        <div class="col-lg-9">
                            <input type="file"  name="meeting_minutes_image" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3"></label>
                        <div class=" col-lg-1">
                            <input type="button" name="start" id="start" value="Start" class="form-control"/>
                        </div>
                        <div class="col-lg-2">
                            <input type="text" name="start_time" id="start_time" class="form-control" required/>
                        </div>
                        <label class="control-label col-lg-1"></label>
                        <div class=" col-lg-1">
                            <input type="button" name="stop" id="stop" value="Stop" disabled="" class="form-control"/>
                        </div>
                        <div class="col-lg-3">
                            <input type="text" name="end_time" id="end_time" class="form-control" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Date</label>
                        <div class="col-lg-3">
                            <input type="datetime" name="date" id="date" class="form-control"/>
                        </div>
                        <label class="control-label col-lg-2">Duration</label>
                        <div class="col-lg-3">
                            <input type="datetime" name="duration" id="duration"  readonly class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Chair</label>
                        <div class="col-lg-6">
                            <input type="text" name="chair" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Subject</label>
                        <div class="col-lg-6">
                            <input type="text" name="subject" class="form-control"/>
                        </div>
                    </div>         
                    <div class="form-group">
                        <label class="control-label col-lg-3">Short Description</label>
                        <div class="col-lg-6">
                            <textarea name="short_description" class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3"> Agenda No 1</label>
                        <div class="col-lg-4">
                            <input type="text" name="area_1" class="form-control" placeholder="Agenda Area" >
                        </div>
                        <div class="col-lg-4">
                            <textarea name="decision_1" class="form-control" placeholder="Decision"></textarea>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3"></label>
                        <div class="col-lg-4">
                            <input type="text" name="responsible_1" class="form-control" placeholder="Responsible" >
                        </div>
                        <div class="col-lg-4">
                            <input type="text" name="target_date_1" class="form-control" placeholder="Target Date" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3"> Agenda No 2</label>
                        <div class="col-lg-4">
                            <input type="text" name="area_2" class="form-control" placeholder="Agenda Area" >
                        </div>
                        <div class="col-lg-4">
                            <textarea name="decision_2" class="form-control" placeholder="Decision"></textarea>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3"></label>
                        <div class="col-lg-4">
                            <input type="text" name="responsible_2" class="form-control" placeholder="Responsible" >
                        </div>
                        <div class="col-lg-4">
                            <input type="text" name="target_date_2" class="form-control" placeholder="Target Date" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3"> Agenda No 3</label>
                        <div class="col-lg-4">
                            <input type="text" name="area_3" class="form-control" placeholder="Agenda Area" >
                        </div>
                        <div class="col-lg-4">
                            <textarea name="decision_3" class="form-control" placeholder="Decision"></textarea>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3"></label>
                        <div class="col-lg-4">
                            <input type="text" name="responsible_3" class="form-control" placeholder="Responsible" >
                        </div>
                        <div class="col-lg-4">
                            <input type="text" name="target_date_3" class="form-control" placeholder="Target Date" >
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-lg-3">Remarks</label>
                        <div class="col-lg-6">
                            <textarea name="remarks" class="form-control" rows="6"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Meeting Minutes File</label>
                        <div class="col-lg-9">
                            <input type="file"  name="meeting_minutes_file">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">Publication Status</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="publication_status">
                                <option> --- Select Publication Status --- </option>
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <input type="submit" name="btn" value="Save Meeting Minutes"  class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function demo() {

        date.style.backgroundColor = 'pink';
        var myDate = new Date();
//        var dd = myDate.getDate();
//        var mm = myDate.getMonth();
//        var yy = myDate.getFullYear();
//        
        var h = myDate.getHours();
        var m = myDate.getUTCMinutes();
        var s = myDate.getSeconds();
        var time = h + ':' + m + ':' + s;
//        document.getElementById('date').value = yy+'-'+mm+'-'+dd+'  '+ h +':'+ m +':' + s;
        document.getElementById('date').value = myDate.toDateString().concat(' ').concat(time);
//            document.getElementById('date').value = myDate.toUTCString();
    }
    setInterval(demo, 1000);

    var start = document.getElementById('start');
    start.onclick = function start_time() {
        var myDate = new Date();
        document.getElementById("start").disabled = true;
        document.getElementById("stop").disabled = false;
        var h = myDate.getHours();
        var m = myDate.getUTCMinutes();
        var s = myDate.getSeconds();
        var time = h + ':' + m + ':' + s;
        document.getElementById('start_time').value = time;
    };

    var stop = document.getElementById('stop');
    stop.onclick = function end_time() {
        var myDate = new Date();
        document.getElementById("stop").disabled = true;

        var h = myDate.getHours();
        var m = myDate.getUTCMinutes();
        var s = myDate.getSeconds();
        var time = h + ':' + m + ':' + s;
        document.getElementById('end_time').value = time;
    };


    function duration() {
        var duration = document.getElementById('duration');
        duration.style.backgroundColor = 'pink';
        
        var startVal = document.getElementById('start_time').value.split(':');
        var stopVal = document.getElementById('end_time').value.split(':');
        var h = (parseInt(stopVal[0]) - parseInt(startVal[0])) * 3600;
        var m = (parseInt(stopVal[1]) - parseInt(startVal[1])) * 60;
        var s = (parseInt(stopVal[2]) - parseInt(startVal[2]));
        var dur = h + m + s;

        var hOut = Math.floor(dur / 3600);
        var mOut = Math.floor((dur - hOut * 3600) / 60);
        var sOut = dur - hOut * 3600 - mOut * 60;
        var result = hOut + ':' + mOut + ':' + sOut;
        if (!isNaN(sOut))
            document.getElementById('duration').value = result;
    };
    setInterval(duration, 1000);

</script>