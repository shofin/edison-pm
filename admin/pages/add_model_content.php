<?php
$message = '';

$query_project_owner = $obj_project_owner->select_all_project_owner_info();
$query_odm = $obj_odm->select_all_odm_info();

if (isset($_POST['btn'])) {
    $message = $obj_models->save_models_info($_POST);
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p class="text-center text-success lead">Add New Model</p>
                <h3 class="text-center text-success lead"><?php echo $message; ?></h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="" method="post">
                    <div class="form-group">
                        <label class="control-label col-lg-3">Model Name</label>
                        <div class="col-lg-9">
                            <input type="text" name="model_name" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Phone Type</label>
                        <div class="col-lg-9">
                            <input type="text" name="phone_type" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Project Owner Name</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="project_owner_id">
                                
                                <option> --- Select Project Owner --- </option>
                               <?php  while ( $all_project_owner = mysqli_fetch_assoc($query_project_owner))  { ?>
                                <option value="<?php echo $all_project_owner['project_owner_id']; ?>"><?php echo $all_project_owner['project_owner_name']; ?></option>
                               <?php };?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3"> ODM Name</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="odm_id">
                                
                                <option> --- Select ODM --- </option>
                               <?php  while ( $all_odm = mysqli_fetch_assoc($query_odm))  { ?>
                                <option value="<?php echo $all_odm['odm_id']; ?>"><?php echo $all_odm['odm_name']; ?></option>
                               <?php };?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Spec and NPD</label>
                        <div class="col-lg-9">
                            <input type="text" name="spec_and_npd" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">PO Date</label>
                        <div class="col-lg-9">
                            <input type="date" name="po_date" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Launching Year</label>
                        <div class="col-lg-9">
                            <input type="text" name="launching_year" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <input type="submit" name="btn" value="Save Model Info" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>