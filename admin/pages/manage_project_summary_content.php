<?php
$message = '';

if (isset($_GET['status'])) {
    $project_summary_id = $_GET['id'];
    if ($_GET['status'] == 'unpublished') {
        $message = $obj_project_summary->unpublished_project_summary_info_by_id($project_summary_id);
    } else if ($_GET['status'] == 'published') {
        $message = $obj_project_summary->published_project_summary_info_by_id($project_summary_id);
    } else if ($_GET['status'] == 'delete') {
        $obj_project_summary = $_GET['id'];
        $message = $obj_project_summary->delete_project_summary_info_by_id($project_summary_id);
    }
}


$query_result = $obj_project_summary->select_all_published_project_summary_info();
//while ($all_project_summary = mysqli_fetch_assoc($query_result)) {
//    echo '<pre>';
//    print_r($all_project_summary); 
//}
//exit();
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header text-center text-success">
            <?php echo $message; ?>
            <?php
            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            }
            ?>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading text-center lead">
                All Project Summary Information Goes Here<br>
                <a href="manage_project_summary.php" class="btn btn-primary" title="Refresh" >
                    <span class="glyphicon glyphicon-refresh"></span>
                </a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">

                    <thead>
                        <tr>
                            <th>SL NO</th>
                            <th>Project Name</th>

                            <th>ODM Name</th>
                            <th>Developer Name</th>
                            <th>Project Owner</th>

                            <th>Total Issue</th>
                            <th>Resolve Issue</th>

                            <th>Confirm soft. Version</th>
                            <th>Confirmation Date</th>
                            <th>Shipment Date</th>

                            <th>Publication Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($all_project_summary_info = mysqli_fetch_assoc($query_result)) {
                            extract($all_project_summary_info);
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $project_name; ?></td>
                                <td><?php echo $odm_name; ?></td>
                                <td><?php echo $developer_name; ?></td>
                                <td><?php echo $project_owner_name; ?></td>



                                <td><?php echo $total_issue; ?></td>
                                <td><?php echo $resolve_issue; ?></td>
                                <td><?php
                                    if (!empty($confirm_soft_version)) {
                                        echo 'V'.$confirm_soft_version;
                                    }else{
                                        echo 'Ongoing';
                                    }
                                    ?></td>
                                <td><?php echo $confirmation_date; ?> <br> yyyy-mm-dd</td>
                                <td><?php echo $shipment_date; ?></td>

                                <td class="center">
                                    <?php
                                    if ($publication_status == 1) {
                                        echo 'Published';
                                    } else {
                                        echo 'Not Published';
                                    }
                                    ?></td>
                                <td class="center">
                                    <a href="project_details.php?id=<?php echo $project_summary_id; ?>" class="btn btn-primary" title="View" >
                                        <span class="glyphicon glyphicon-zoom-in"></span>
                                    </a>
    <?php if ($publication_status == 1) { ?>
                                        <a href="?status=unpublished&&id=<?php echo $project_summary_id; ?>" class="btn btn-primary" title="Unpublished">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
    <?php } else { ?>
                                        <a href="?status=published&&id=<?php echo $project_summary_id; ?>" class="btn btn-danger" title="Published">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
    <?php } ?>
                                    <a href="edit_flagship_model.php?id=<?php echo $project_summary_id; ?>" class="btn btn-success" title="Edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a href="?status=delete&&id=<?php echo $project_summary_id; ?>" class="btn btn-danger" title="Delete" onclick="return check_delete_status();">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>