<?php
$message = '';
$query_result = $obj_odm->select_all_odm_info();
$query_project = $obj_project->select_all_project_info();
$query_project_owner = $obj_project_owner->select_all_project_owner_info();


if (isset($_POST['btn'])) {
    $message = $obj_common_issue->save_common_issue_info($_POST);
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p class="text-center text-success lead">Common Issue Form</p>
                <h3 class="text-center text-success lead"><?php echo $message; ?></h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-lg-3">Project Name</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="project_id" required=""> 
                                <option> --- Select Project Name --- </option>
                               <?php  while ( $all_project_info = mysqli_fetch_assoc($query_project))  { ?>
                                <option value="<?php echo $all_project_info['project_id']; ?>"><?php echo $all_project_info['project_name']; ?></option>
                               <?php };?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-lg-3">Project Owner</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="project_owner_id">
                                <option> --- Select Project Owner --- </option>
                                <?php  while ( $all_project_owner_info = mysqli_fetch_assoc($query_project_owner))  { ?>
                                <option value="<?php echo $all_project_owner_info['project_owner_id']; ?>"><?php echo $all_project_owner_info['project_owner_name']; ?></option>
                               <?php };?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">ODM Name</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="odm_id">
                                
                                <option> --- Select ODM Name --- </option>
                               <?php  while ( $all_odm_info = mysqli_fetch_assoc($query_result))  { ?>
                                <option value="<?php echo $all_odm_info['odm_id']; ?>"><?php echo $all_odm_info['odm_name']; ?></option>
                               <?php };?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Chipset</label>
                        <div class="col-lg-9">
                            <input type="text" name="chipset" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Subject</label>
                        <div class="col-lg-9">
                            <input type="text" name="subject" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Issue Details</label>
                        <div class="col-lg-9">
                            <textarea name="issue_details" class="form-control" rows="6"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Remarks</label>
                        <div class="col-lg-9">
                            <textarea name="remarks" class="form-control" rows="6"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <input type="submit" name="btn" value="Save Common Issue" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>