<?php 
$project_owner_id = $_GET['id'];
$query_result = $obj_project_owner->edit_project_owner_info_by_id($project_owner_id);
$project_owner_info = mysqli_fetch_assoc($query_result);
extract($project_owner_info);

if (isset($_POST['btn'])) {
    $obj_project_owner->update_project_owner_info_by_id($_POST);
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p class="text-center text-success lead">Edit Project Owner Form</p>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="" method="post" name="edit_project_owner_form">
                    <div class="form-group">
                        <label class="control-label col-lg-3">Project Owner Name</label>
                        <div class="col-lg-9">
                            <input type="text" name="project_owner_name" value="<?php echo $project_owner_name; ?>" class="form-control" required>
                            <input type="hidden" name="project_owner_id" value="<?php echo $project_owner_id; ?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Edison ID</label>
                        <div class="col-lg-9">
                            <input type="number" name="edison_id" value="<?php echo $edison_id; ?>"class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Email Address</label>
                        <div class="col-lg-9">
                            <input type="email" name="email_address" value="<?php echo $email_address; ?>"class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Project Owner Description</label>
                        <div class="col-lg-9">
                            <textarea name="project_owner_description" class="form-control" rows="6"><?php echo $project_owner_description; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Publication Status</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="publication_status">
                                <option> --- Select Publication Status --- </option>
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <input type="submit" name="btn" value="Update Project Owner Info" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    document.forms['edit_project_owner_form'].elements['publication_status'].value = '<?php echo $publication_status; ?>';
</script>