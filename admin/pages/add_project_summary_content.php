<?php
$message = '';
$query_result = $obj_odm->select_all_odm_info();
$query_project = $obj_project->select_all_project_info();
$query_project_owner = $obj_project_owner->select_all_project_owner_info();
$query_developer = $obj_developer->select_all_published_developer_info();
$query_tester1 = $obj_tester->select_all_published_tester_info();
$query_tester2 = $obj_tester->select_all_published_tester_info();
$query_tester3 = $obj_tester->select_all_published_tester_info();
$query_tester4 = $obj_tester->select_all_published_tester_info();


if (isset($_POST['btn'])) {
    $message = $obj_project_summary->save_project_summary_info($_POST);
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p class="text-center text-success lead">Project Summary Form</p>
                <h3 class="text-center text-success lead"><?php echo $message; ?></h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-lg-3">Project Name</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="project_id">
                                
                                <option> --- Select Project Name --- </option>
                               <?php  while ( $all_project_info = mysqli_fetch_assoc($query_project))  { ?>
                                <option value="<?php echo $all_project_info['project_id']; ?>"><?php echo $all_project_info['project_name']; ?></option>
                               <?php };?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Project Type</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="project_type">
                                <option> --- Select Project Type --- </option>
                                <option value="1">Smart phone/Tab</option>
                                <option value="2">Feature Phone</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">ODM Name</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="odm_id">
                                
                                <option> --- Select ODM Name --- </option>
                               <?php  while ( $all_odm_info = mysqli_fetch_assoc($query_result))  { ?>
                                <option value="<?php echo $all_odm_info['odm_id']; ?>"><?php echo $all_odm_info['odm_name']; ?></option>
                               <?php };?>
                            </select>
                        </div>
                    </div>

                    <!--                    <div class="form-group">
                                            <label class="control-label col-lg-3">ODM Name</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="odm_name" class="form-control" required>
                                            </div>
                                        </div>-->
                    <div class="form-group">
                        <label class="control-label col-lg-3">Developer Name</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="developer_id">
                                
                                <option> --- Select Developer Name --- </option>
                               <?php  while ( $all_published_developer_info = mysqli_fetch_assoc($query_developer))  { ?>
                                <option value="<?php echo $all_published_developer_info['developer_id']; ?>"><?php echo $all_published_developer_info['developer_name']; ?></option>
                               <?php };?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">Project Owner</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="project_owner_id">
                                <option> --- Select Project Owner --- </option>
                                <?php  while ( $all_project_owner_info = mysqli_fetch_assoc($query_project_owner))  { ?>
                                <option value="<?php echo $all_project_owner_info['project_owner_id']; ?>"><?php echo $all_project_owner_info['project_owner_name']; ?></option>
                               <?php };?>
                            </select>
                        </div>
                    </div>
                    <!--                    <div class="form-group">
                                            <label class="control-label col-lg-3">Project Owner</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="project_owner" class="form-control" required>
                                            </div>
                                        </div>-->
                    <div class="form-group">
                        <label class="control-label col-lg-3">Tester 1</label>
                        <div class="col-lg-2">
                            <select class="form-control" name="tester1_id">
                                Select Tester 1
<!--                                <option value="">Tester 1-- </option>-->
                                <?php  while ( $all_published_tester_info_1 = mysqli_fetch_assoc($query_tester1))  { ?>
                                <option value="<?php echo $all_published_tester_info_1['tester_id']; ?>"><?php echo $all_published_tester_info_1['tester_name']; ?></option>
                               <?php };?>
                            </select>
                        </div>
                        <label class="control-label col-lg-3">Tester 2</label>
                        <div class="col-lg-2">
                            <select class="form-control" name="tester2_id">
<!--                                <option value="">Tester 2-- </option>-->
                                <?php  while ( $all_published_tester_info_2 = mysqli_fetch_assoc($query_tester2))  { ?>
                                <option value="<?php echo $all_published_tester_info_2['tester_id']; ?>"><?php echo $all_published_tester_info_2['tester_name']; ?></option>
                               <?php };?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-lg-3">Tester 3</label>
                        <div class="col-lg-2">
                            <select class="form-control" name="tester3_id" value>
<!--                                <option value="">Tester 3-- </option>-->
                                <?php  while ( $all_published_tester_info_3 = mysqli_fetch_assoc($query_tester3))  { ?>
                                <option value="<?php echo $all_published_tester_info_3['tester_id']; ?>"><?php echo $all_published_tester_info_3['tester_name']; ?></option>
                               <?php };?>
                            </select>
                        </div>
                        <label class="control-label col-lg-3">Tester 4</label>
                        <div class="col-lg-2">
                            <select class="form-control" name="tester4_id">
<!--                                <option value="1">Tester 4-- </option>-->
                                <?php  while ( $all_published_tester_info_4 = mysqli_fetch_assoc($query_tester4))  { ?>
                                <option value="<?php echo $all_published_tester_info_4['tester_id']; ?>"><?php echo $all_published_tester_info_4['tester_name']; ?></option>
                               <?php };?>
                            </select>
                        </div>
                    </div>
<!--                    <div class="form-group">
                        <label class="control-label col-lg-3">Project Tester</label>
                        <fieldset>
                            <div class="item" style="padding-left: 2;">
                                <input type="checkbox" id="a" name="tester[]" value="safiul@edisoon.com">
                                <label for="a">Safiul Islam</label>
                                <input type="checkbox" id="b" name="tester[]" value="najim@edisoon.com">
                                <label for="b">Najmul Islam</label>
                                <input type="checkbox" id="c" name="tester[]" value="raza@edisoon.com">
                                <label for="c">Raja Hassan</label>
                                <input type="checkbox" id="d" name="tester[]" value="ainul@edisoon.com">
                                <label for="d">Ainul Huda</label>
                            </div>

                        </fieldset>
                    </div>-->
                    <div class="form-group">
                        <label class="control-label col-lg-3">Hardware Version</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="hardware_version">
                                <option> --- Select Hardware Version --- </option>
                                <option value="1">V1</option>
                                <option value="2">V2</option>
                                <option value="3">V3</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Current Soft. Version</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="current_soft_version">
                                <option> --- Select Current Soft. Version --- </option>
                                <option value="1">V1</option>
                                <option value="2">V2</option>
                                <option value="3">V3</option>
                                <option value="4">V4</option>
                                <option value="5">V5</option>
                                <option value="6">V6</option>
                                <option value="7">V7</option>
                                <option value="8">V8</option>
                                <option value="9">V9</option>
                                <option value="10">V10</option>
                            </select>
                        </div>
                    </div>
                    <!--                    <div class="form-group">
                                            <label class="control-label col-lg-3">Current Soft. Version</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="current_soft_version" class="form-control" required>
                                            </div>
                                        </div>-->
                    <div class="form-group">
                        <label class="control-label col-lg-3"> Total Issue</label>
                        <div class="col-lg-2">
                            <input type="number" name="total_issue" class="form-control" >
                        </div>
                        <label class="control-label col-lg-3"> Resolve Issue</label>
                        <div class="col-lg-2">
                            <input type="number" name="resolve_issue" class="form-control" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">Open Issue</label>
                        <div class="col-lg-2">
                            <input type="number" name="open_issue" class="form-control" >
                        </div>
                        <label class="control-label col-lg-3">Normal Issue</label>
                        <div class="col-lg-2">
                            <input type="number" name="normal_issue" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3"> After Sales Issue</label>
                        <div class="col-lg-2">
                            <input type="number" name="after_sales_issue" class="form-control" >
                        </div>
                        <label class="control-label col-lg-3"> After Sales Resolve Issue</label>
                        <div class="col-lg-2">
                            <input type="number" name="after_sales_resolve_issue" class="form-control" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3"> Confirm Soft. version</label>
                        <div class="col-lg-9">
                            <input type="text" name="confirm_soft_version" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3"> Confirmation Date</label>
                        <div class="col-lg-2">
                            <input type="date" class="" name="confirmation_date" placeholder="YYYY-MM-DD" class="form-control" >
                        </div>
                        <label class="control-label col-lg-3"> Shipment Date</label>
                        <div class="col-lg-2">
                            <input type="date" class="" name="shipment_date" placeholder="YYYY-MM-DD" class="form-control" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">Any Difficulties?</label>
                        <div class="col-lg-9">
                            <textarea name="any_difficulties" class="form-control" rows="6"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Models Image</label>
                        <div class="col-lg-9">
                            <input type="file"  name="models_image" required="">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-lg-3">Final PDS</label>
                        <div class="col-lg-9">
                            <input type="file"  name="final_pds">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-lg-3">Publication Status</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="publication_status">
                                <option> --- Select Publication Status --- </option>
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <input type="submit" name="btn" value="Save Projecct Summary" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>