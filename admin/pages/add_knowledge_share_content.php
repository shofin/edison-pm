<?php
$message = '';

if (isset($_POST['btn'])) {
    $message = $obj_meetings->save_knowledge_share_info($_POST);
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p class="text-center text-success lead">New Knowledge Share Form</p>
                <h3 id="test" class="text-center text-success lead"><?php echo $message; ?></h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">

                    <div class="form-group">
                        <label class="control-label col-lg-3">Date</label>
                        <div class="col-lg-3">
                            <input type="datetime" name="date_time" id="date" class="form-control"/>
                        </div>
                        <label class="control-label col-lg-2">Publisher</label>
                        <div class="col-lg-3">
                            <input type="text" name="publisher_name" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Title</label>
                        <div class="col-lg-6">
                            <input type="text" name="knowledge_share_title" class="form-control" required=""/>
                        </div>
                    </div>         
                    <div class="form-group">
                        <label class="control-label col-lg-3">Contents</label>
                        <div class="col-lg-6">
                            <textarea name="knowledge_share_contents" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">File</label>
                        <div class="col-lg-9">
                            <input type="file"  name="knowledge_share_file" required=""/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">Publication Status</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="publication_status">
                                <option> --- Select Publication Status --- </option>
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-6">
                            <input type="submit" name="btn" value="Save Knowledge Share"  class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function demo() {

        date.style.backgroundColor = 'pink';
        var myDate = new Date();
//        var dd = myDate.getDate();
//        var mm = myDate.getMonth();
//        var yy = myDate.getFullYear();
//        
        var h = myDate.getHours();
        var m = myDate.getUTCMinutes();
        var s = myDate.getSeconds();
        var time = h + ':' + m + ':' + s;
//        document.getElementById('date').value = yy+'-'+mm+'-'+dd+'  '+ h +':'+ m +':' + s;
        document.getElementById('date').value = myDate.toDateString().concat(' ').concat(time);
//            document.getElementById('date').value = myDate.toUTCString();
    }
    setInterval(demo, 1000);

</script>