<?php
$message = '';

if (isset($_GET['status'])) {
    $team_leader_id = $_GET['id'];
    if ($_GET['status'] == 'unpublished') {
        $message = $obj_team_leader->unpublished_team_leader_info_by_id($team_leader_id);
    } else if ($_GET['status'] == 'published') {
        $message = $obj_team_leader->published_team_leader_info_by_id($team_leader_id);
    } else if ($_GET['status'] == 'delete') {
        $message = $obj_team_leader->delete_team_leader_info_by_id($team_leader_id);
    }
}


$query_result = $obj_team_leader->select_all_team_leader_info();
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header text-center text-success">
            <?php echo $message; ?>
            <?php
            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            }
            ?>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading text-center lead">
                All Team  Leader Information Goes Here
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SL NO</th>
                            <th>Team Leader Name</th>
                            <th>Nick Name</th>
                            <th>Designation</th>
                            <th>Employee ID</th>
                            <th>Career Short Description</th>
                            <th>Team Leader Image</th>
                            <th>Publication Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($team_leader_info = mysqli_fetch_assoc($query_result)) {
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $team_leader_info['team_leader_name']; ?></td>
                                <td><?php echo $team_leader_info['nick_name']; ?></td>
                                <td><?php echo $team_leader_info['team_leader_designation']; ?></td>
                                <td><?php echo $team_leader_info['employe_id']; ?></td>
                                <td><?php echo $team_leader_info['career_short_description']; ?></td>
                                <td><img src="<?php echo $team_leader_info['team_leader_image']; ?>" alt="" height="100" width="100"></td>

                                <td class="center">
                                    <?php
                                    if ($team_leader_info['publication_status'] == 1) {
                                        echo 'Published';
                                    } else {
                                        echo 'Unpublished';
                                    }
                                    ?></td>
                                <td class="center">
                                    <?php if ($team_leader_info['publication_status'] == 1) { ?>
                                        <a href="?status=unpublished&&id=<?php echo $team_leader_info['team_leader_id']; ?>" class="btn btn-primary" title="Unpublished">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    <?php } else { ?>
                                        <a href="?status=published&&id=<?php echo $team_leader_info['team_leader_id']; ?>" class="btn btn-danger" title="Published">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    <?php } ?>
                                    <a href="edit_team_leader.php?id=<?php echo $team_leader_info['team_leader_id']; ?>" class="btn btn-success" title="Edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a href="?status=delete&&id=<?php echo $team_leader_info['team_leader_id']; ?>" class="btn btn-danger" title="Delete" onclick="return check_delete_status();">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>