<?php
$message = '';

if (isset($_GET['status'])) {
    $flagship_id = $_GET['id'];
    if ($_GET['status'] == 'unpublished') {
        $message = $obj_flagship->unpublished_flagship_info_by_id($flagship_id);
    } else if ($_GET['status'] == 'published') {
        $message = $obj_flagship->published_flagship_info_by_id($flagship_id);
    } else if ($_GET['status'] == 'delete') {
        $flagship_id = $_GET['id'];
        $message = $obj_flagship->delete_flagship_info_by_id($flagship_id);
    }
}


$query_result = $obj_flagship->select_all_flagship_info();
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header text-center text-success">
            <?php echo $message; ?>
            <?php
            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            }
            ?>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading text-center lead">
                All Flagship Models Information Goes Here
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SL NO</th>
                            <th>Flagship Title</th>
                            <th>flagship model name</th>
                            <th>short description</th>
                            <th>key_feature</th>
                            <th>Flagship image</th>
                            <th>Flagship video</th>
                            <th>Publication Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($flagship_info = mysqli_fetch_assoc($query_result)) {
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $flagship_info['flagship_title']; ?></td>
                                <td><?php echo $flagship_info['flagship_model_name']; ?></td>
                                <td><?php echo $flagship_info['short_description']; ?></td>
                                <td><?php echo $flagship_info['key_feature']; ?></td>
                                <td><img src="<?php echo $flagship_info['flagship_image']; ?>" alt="" height="100" width="100"></td>
                                <td><div class="embed-responsive embed-responsive-4by3">
                                        <?php echo $flagship_info['flagship_video']; ?>
                                    </div>
                                <td class="center"><?php
                                    if ($flagship_info['publication_status'] == 1) {
                                        echo 'Published';
                                    } else {
                                        echo 'Unpublished';
                                    }
                                    ?></td>
                                <td class="center">
                                    <?php if ($flagship_info['publication_status'] == 1) { ?>
                                        <a href="?status=unpublished&&id=<?php echo $flagship_info['flagship_id']; ?>" class="btn btn-primary" title="Unpublished">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    <?php } else { ?>
                                        <a href="?status=published&&id=<?php echo $flagship_info['flagship_id']; ?>" class="btn btn-danger" title="Published">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    <?php } ?>
                                    <a href="edit_flagship_model.php?id=<?php echo $flagship_info['flagship_id']; ?>" class="btn btn-success" title="Edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a href="?status=delete&&id=<?php echo $flagship_info['flagship_id']; ?>" class="btn btn-danger" title="Delete" onclick="return check_delete_status();">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>