<?php 
$message = '';

if (isset($_POST['btn'])) {
    $message = $obj_pds->save_pds_info($_POST);
}

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <p class="text-center text-success lead">Add PDS Files Form</p>
        <h3 class="text-center text-success lead"><?php echo $message; ?></h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label class="control-label col-lg-3">Final PDS</label>
                <div class="col-lg-9">
                    <input type="file"  name="pds_files">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">Final User Manual</label>
                <div class="col-lg-9">
                    <input type="file"  name="user_manual">
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-3 col-lg-9">
                    <input type="submit" name="btn" value="Save PDS" class="btn btn-primary btn-block">
                </div>
            </div>
        </form>
    </div>
</div>