<?php
$message = '';


if (isset($_GET['status'])) {
    $user_id = $_GET['id'];
    if ($_GET['status'] == 'not_approve') {
        $message = $obj_user->not_approve_user_info_by_id($user_id);
    } else if ($_GET['status'] == 'approve') {
        $message = $obj_user->approve_user_info_by_id($user_id);
    } else if ($_GET['status'] == 'delete') {
        $message = $obj_user->delete_user_info_by_id($user_id);
    } else if ($_GET['status'] == 'normal_user') {
        $message = $obj_user->set_normal_user_info_by_id($user_id);
    } else if ($_GET['status'] == 'pm_member') {
        $message = $obj_user->set_pm_member_info_by_id($user_id);
    }
}


$query_result = $obj_user->select_all_user_info();
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header text-center text-success">
            <?php echo $message; ?>
            <?php
            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            }
            ?>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading text-center lead">
                All User Information Goes Here
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SL NO</th>
                            <th>User Name</th>
                            <th>Employee ID</th>
                            <th>Phone Number</th>
                            <th>Email Address</th>
                            <th>User Image</th>
                            <th>Approve Status</th>
                            <th>PM</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($all_user_info = mysqli_fetch_assoc($query_result)) {
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $all_user_info['user_name']; ?></td>
                                <td><?php echo $all_user_info['employe_id']; ?></td>
                                <td><?php echo $all_user_info['phone_number']; ?></td>
                                <td><?php echo $all_user_info['email_address']; ?></td>

                                <td><img src="<?php echo '../' . $all_user_info['user_image']; ?>" alt="" height="80" width="90">
                                    <hr>
                                    <img src="<?php echo $all_user_info['user_image']; ?>" alt="" height="60" width="60">
                                </td>

                                <td class="center">
                                    <?php
                                    if ($all_user_info['approve_status'] == 1) {
                                        echo 'Approved';
                                    } else {
                                        echo 'Not Approve';
                                    }
                                    ?></td>
                                <td class="center">
                                    <?php
                                    if ($all_user_info['pm_member'] == 1) {
                                        echo 'PM Member';
                                    } else {
                                        echo 'Normal User';
                                    }
                                    ?></td>
                                <td class="center">
                                    <?php if ($all_user_info['approve_status'] == 1) { ?>
                                        <a href="?status=not_approve&&id=<?php echo $all_user_info['user_id']; ?>" class="btn btn-primary" title="Not-Approve">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    <?php } else { ?>
                                        <a href="?status=approve&&id=<?php echo $all_user_info['user_id']; ?>" class="btn btn-danger" title="Approve">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    <?php } ?>

                                    <?php if ($all_user_info['pm_member'] == 1) { ?>
                                        <a href="?status=normal_user&&id=<?php echo $all_user_info['user_id']; ?>" class="btn btn-primary" title="Normal user">
                                            <span class="glyphicon glyphicon-arrow-left"></span>
                                        </a>
                                    <?php } else { ?>
                                        <a href="?status=pm_member&&id=<?php echo $all_user_info['user_id']; ?>" class="btn btn-danger" title="PM Member">
                                            <span class="glyphicon glyphicon-arrow-right"></span>
                                        </a>
                                    <?php } ?>

                                    <a href="#?id=<?php echo $all_user_info['user_id']; ?>" class="btn btn-success" title="Edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a href="?status=delete&&id=<?php echo $all_user_info['user_id']; ?>" class="btn btn-danger" title="Delete" onclick="return check_delete_status();">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>