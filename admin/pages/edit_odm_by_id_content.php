<?php 
$message='';
$odm_id = $_GET['id']; 

$query_odm = $obj_odm->select_odm_by_id($odm_id);
$odm_info_by_id = mysqli_fetch_assoc($query_odm);
extract($odm_info_by_id);

if (isset($_POST['btn'])) {
    $obj_odm->update_odm_info_by_id($_POST);
}

?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p class="text-center text-success lead">Update ODM Info</p>
                <h3 class="text-center text-success lead"><?php echo $message; ?></h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="" name="odm_info" method="post">
                    <div class="form-group">
                        <label class="control-label col-lg-3">ODM Name</label>
                        <div class="col-lg-9">
                            <input type="text" name="odm_name" value="<?php echo $odm_name;?>" class="form-control" required>
                            <input type="hidden" name="odm_id" value="<?php echo $odm_id;?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Contact 1</label>
                        <div class="col-lg-9">
                            <input type="text" name="contact_1" value="<?php echo $contact_1;?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Contact 2</label>
                        <div class="col-lg-9">
                            <input type="text" name="contact_2" value="<?php echo $contact_2;?>"class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">ODM Description</label>
                        <div class="col-lg-9">
                            <textarea name="odm_description" class="form-control" rows="6"><?php echo $odm_description;?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Publication Status</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="publication_status">
                                <option> --- Select Publication Status --- </option>
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <input type="submit" name="btn" value="Update ODM Info" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    document.forms['odm_info'].elements['publication_status'].value='<?php echo $publication_status; ?>';
</script>