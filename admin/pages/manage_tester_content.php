<?php
$message= '';

if(isset($_GET['status'])) {
    $tester_id=$_GET['id'];
    if($_GET['status'] == 'unpublished') {
        $message = $obj_tester->unpublished_tester_info_by_id($tester_id);
    }
    else if ($_GET['status'] == 'published') {
         $message = $obj_tester->published_tester_info_by_id($tester_id);
    }
    else if ($_GET['status'] == 'delete') {
         $message = $obj_tester->delete_tester_info_by_id($tester_id);
    }
}
$query_result = $obj_tester->select_all_tester_info();
?>
<div class="row">  
        <h3 class="page-header text-center text-success">
            <?php echo $message; ?>
        </h3>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading text-center lead">
                All Tester Information Goes Here
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SL NO</th>
                            <th>Tester Name</th>
                            <th>Email</th>
                            <th>Description</th>
                            <th>Publication Status</th>
                            
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($all_tester = mysqli_fetch_assoc($query_result)) {
                            extract($all_tester);
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $tester_name; ?></td>
                                <td><?php echo $tester_email_address; ?></td>
                                <td><?php echo $tester_description; ?></td>
                                <td class="center"><?php
                                    if ($publication_status == 1) {
                                        echo 'Published';
                                    } else {
                                        echo 'Unpublished';
                                    }
                                    ?></td>
                                
                                <td class="center">
                                    <?php if ($publication_status == 1) { ?>
                                        <a href="?status=unpublished&&id=<?php echo $tester_id; ?>" class="btn btn-primary" title="Unpublished">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    <?php } else { ?>
                                        <a href="?status=published&&id=<?php echo $tester_id; ?>" class="btn btn-danger" title="Published">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    <?php } ?>
                                    <a href="#?id=<?php echo $$tester_id; ?>" class="btn btn-success" title="Edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a href="?status=delete&&id=<?php echo $tester_id; ?>" class="btn btn-danger" title="Delete" onclick="return check_delete_status();">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>