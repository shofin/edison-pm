<?php
$message = '';

if (isset($_POST['btn'])) {
    $message = $obj_meetings->save_news_feed($_POST);
}
?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p class="text-center text-success lead">New News</p>
                <h3 id="test" class="text-center text-success lead"><?php echo $message; ?></h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="" method="post">

                    <div class="form-group">
                        <label class="control-label col-lg-3">Date</label>
                        <div class="col-lg-3">
                            <input type="text" name="news_date_time" id="date" class="form-control"/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-lg-3">Title</label>
                        <div class="col-lg-6">
                            <input type="text" name="news_title" class="form-control"/>
                        </div>
                    </div>        
                    <div class="form-group">
                        <label class="control-label col-lg-3">News Contents</label>
                        <div class="col-lg-6">
                            <textarea name="news_contents" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Publication Status</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="publication_status">
                                <option> --- Select Publication Status --- </option>
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <input type="submit" name="btn" value="Save News Feed"  class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function demo() {

        date.style.backgroundColor = 'pink';
        var myDate = new Date();
//        var dd = myDate.getDate();
//        var mm = myDate.getMonth();
//        var yy = myDate.getFullYear();
//        
        var h = myDate.getHours();
        var m = myDate.getUTCMinutes();
        var s = myDate.getSeconds();
        var time = h + ':' + m + ':' + s;
//        document.getElementById('date').value = yy+'-'+mm+'-'+dd+'  '+ h +':'+ m +':' + s;
        document.getElementById('date').value = myDate.toDateString().concat(' ').concat(time);
//            document.getElementById('date').value = myDate.toUTCString();
    }
    setInterval(demo, 1000);

</script>

