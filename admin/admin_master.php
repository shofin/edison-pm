<?php
ob_start();
session_start();
require '../classes/login_signup.php';

$obj_login_signup = new Login_signup();

require '../classes/project_owner.php';
$obj_project_owner = new Project_owner();

require '../classes/tester.php';
$obj_tester = new Tester();

require '../classes/pds.php';
$obj_pds = new Pds();

require '../classes/odm.php';
$obj_odm = new Odm();

require '../classes/models.php';
$obj_models = new Models();

require '../classes/developer.php';
$obj_developer = new Developer();

require '../classes/project.php';
$obj_project = new Project();

require '../classes/user.php';
$obj_user = new User();

require '../classes/project_summary.php';
$obj_project_summary = new Project_summary();

require '../classes/common_issue.php';
$obj_common_issue = new Common_issue();

require '../classes/normal_issue.php';
$obj_normal_issue = new Normal_issue();

require '../classes/meetings.php';
$obj_meetings = new Meetings();

require '../classes/slider.php';
$obj_slider = new Slider();

require '../classes/flagship.php';
$obj_flagship = new Flagship();

require '../classes/about_us.php';
$obj_about_us = new About_us();

require '../classes/team_leader.php';
$obj_team_leader = new Team_leader();


if ($_SESSION['admin_id'] == NULL) {
    header('Location: index.php');
}


if (isset($_GET['status'])) {
    if ($_GET['status'] == 'logout') {
        $obj_login_signup->admin_logout();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Edison PM Admin</title>
        <!-- Bootstrap Core CSS -->
        <link href="../assets/backend/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- MetisMenu CSS -->
        <link href="../assets/backend/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
        <link href="../assets/backend/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
        <!-- DataTables Responsive CSS -->
        <link href="../assets/backend/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../assets/backend/dist/css/sb-admin-2.css" rel="stylesheet">
        <!-- Morris Charts CSS -->
        <link href="../assets/backend/vendor/morrisjs/morris.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="../assets/backend/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <script>
            function check_delete_status() {
                var check = confirm('Are you sure to delete this !');
                if (check) {
                    return true;
                } else {
                    return false;
                }
            }
        </script>
    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">Edison PM Admin</a>
                </div>
                <!-- /.navbar-header -->
                <?php include './includes/top_menu.php'; ?>
                <!-- /.navbar-top-links -->
                <?php include './includes/sidebar.php'; ?>
                <!-- /.navbar-static-side -->
            </nav>  

            <div id="page-wrapper">
                <?php
                if (isset($pages)) {
                    if ($pages == 'add_project_owner') {
                        include './pages/add_project_owner_content.php';
                    } else if ($pages == 'manage_project_owner') {
                        include './pages/manage_project_owner_content.php';
                    } else if ($pages == 'edit_project_owner') {
                        include './pages/edit_project_owner_content.php';
                    } else if ($pages == 'new_meeting_minutes') {
                        include './pages/new_meeting_minutes_content.php';
                    } else if ($pages == 'manage_meetings') {
                        include './pages/manage_meetings_content.php';
                    } else if ($pages == 'view_meetings') {
                        include './pages/view_meetings_content.php';
                    } else if ($pages == 'add_news_feed') {
                        include './pages/add_news_feed_content.php';
                    } else if ($pages == 'manage_news_feed') {
                        include './pages/manage_news_feed_content.php';
                    } else if ($pages == 'add_knowledge_share') {
                        include './pages/add_knowledge_share_content.php';
                    } else if ($pages == 'manage_knowledge_share') {
                        include './pages/manage_knowledge_share_content.php';
                    } else if ($pages == 'add_common_issue') {
                        include './pages/add_common_issue_content.php';
                    } else if ($pages == 'manage_common_issue') {
                        include './pages/manage_common_issue_content.php';
                    } else if ($pages == 'edit_common_issue') {
                        include './pages/edit_common_issue_content.php';
                    } else if ($pages == 'search_common_issue') {
                        include './pages/search_common_issue_content.php';
                    } else if ($pages == 'add_normal_issue') {
                        include './pages/add_normal_issue_content.php';
                    } else if ($pages == 'manage_normal_issue') {
                        include './pages/manage_normal_issue_content.php';
                    } else if ($pages == 'edit_normal_issue') {
                        include './pages/edit_normal_issue_content.php';
                    } else if ($pages == 'search_normal_issue') {
                        include './pages/search_normal_issue_content.php';
                    } else if ($pages == 'add_files') {
                        include './pages/add_files_content.php';
                    } else if ($pages == 'manage_files') {
                        include './pages/manage_files_content.php';
                    } else if ($pages == 'add_odm') {
                        include './pages/add_odm_content.php';
                    } else if ($pages == 'manage_odm') {
                        include './pages/manage_odm_content.php';
                    } else if ($pages == 'edit_odm_by_id') {
                        include './pages/edit_odm_by_id_content.php';
                    } else if ($pages == 'add_developer') {
                        include './pages/add_developer_content.php';
                    } else if ($pages == 'edit_odm') {
                        include './pages/edit_odm_content.php';
                    } else if ($pages == 'add_tester') {
                        include './pages/add_tester_content.php';
                    } else if ($pages == 'manage_tester') {
                        include './pages/manage_tester_content.php';
                    } else if ($pages == 'add_project') {
                        include './pages/add_project_content.php';
                    } else if ($pages == 'add_user') {
                        include './pages/add_user_content.php';
                    } else if ($pages == 'manage_user') {
                        include './pages/manage_user_content.php';
                    } else if ($pages == 'add_project_summary') {
                        include './pages/add_project_summary_content.php';
                    } else if ($pages == 'manage_project_summary') {
                        include './pages/manage_project_summary_content.php';
                    } else if ($pages == 'add_model') {
                        include './pages/add_model_content.php';
                    } else if ($pages == 'manage_model') {
                        include './pages/manage_model_content.php';
                    } else if ($pages == 'edit_model') {
                        include './pages/edit_model_content.php';
                    } else if ($pages == 'project_details') {
                        include './pages/project_details_content.php';
                    } else if ($pages == 'main_slider') {
                        include './pages/main_slider_content.php';
                    } else if ($pages == 'manage_slider') {
                        include './pages/manage_slider_content.php';
                    } else if ($pages == 'edit_slider') {
                        include './pages/edit_slider_content.php';
                    } else if ($pages == 'add_flagship_models') {
                        include './pages/add_flagship_models_content.php';
                    } else if ($pages == 'manage_flagship_models') {
                        include './pages/manage_flagship_models_content.php';
                    } else if ($pages == 'edit_flagship_model') {
                        include './pages/edit_flagship_model_content.php';
                    } else if ($pages == 'add_about_us') {
                        include './pages/add_about_us_content.php';
                    } else if ($pages == 'manage_about_us') {
                        include './pages/manage_about_us_content.php';
                    } else if ($pages == 'add_team_leader') {
                        include './pages/add_team_leader_content.php';
                    } else if ($pages == 'manage_team_leader') {
                        include './pages/manage_team_leader_content.php';
                    } else if ($pages == 'edit_team_leader') {
                        include './pages/edit_team_leader_content.php';
                    }
                } else {
                    include './pages/home_content.php';
                }
                ?>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- jQuery -->
        <script src="../assets/backend/vendor/jquery/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="../assets/backend/vendor/bootstrap/js/bootstrap.min.js"></script>
        <!-- Metis Menu Plugin JavaScript -->
        <script src="../assets/backend/vendor/metisMenu/metisMenu.min.js"></script>
        <!-- Morris Charts JavaScript -->
        <script src="../assets/backend/vendor/raphael/raphael.min.js"></script>
        <script src="../assets/backend/vendor/morrisjs/morris.min.js"></script>
        <script src="../assets/backend/data/morris-data.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="../assets/backend/vendor/datatables/js/jquery.dataTables.min.js"></script>
        <script src="../assets/backend/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
        <script src="../assets/backend/vendor/datatables-responsive/dataTables.responsive.js"></script>

        <script src="../assets/backend/dist/js/sb-admin-2.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').DataTable({
                    responsive: true
                });
            });

//             $(document).ready(function () {
//                alert('hai');
//            });


        </script>
    </body>
</html>
